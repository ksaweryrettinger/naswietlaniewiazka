#pragma once

#include "wx/wx.h"
#include "cModbus.h"
#include "cMain.h"
#include "constants.h"

class cMain;
class cModbus;

class cResetCoils : public wxFrame
{
public: //constructor
	cResetCoils(cMain*, cModbus*);

private: //event handlers
	void OnButtonResetCoils(wxCommandEvent& evt);
	void OnButtonOK(wxCommandEvent& evt);
	void OnClose(wxCloseEvent& evt);

private: //pointer received in contructor
	cMain* pMain;
	cModbus* mbMaster;

private: //variables and pointers
    int32_t rc;
    uint8_t sumCoils;
    uint8_t zero[MB_NUM_COILS] = { 0 };
	wxFont myFont;
    wxStaticText* txtInformation;
    wxStaticText* txtUserMessage;
    wxButton* btnResetCoils;
    wxButton* btnOK;

	wxDECLARE_EVENT_TABLE();
};

