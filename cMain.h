#pragma once

#include <string.h>
#include <locale>
#include <queue>
#include "wx/wx.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "cModbus.h"
#include "cConstants.h"
#include "cThreadTPG362.h"
#include "cThreadSystemClock.h"
#include "cThreadRefreshDisplay.h"
#include "cInformation.h"
#include "constants.h"

class cEntry;
class cModbus;
class cConstants;
class cInformation;
class cThreadTPG362;
class cThreadSystemClock;
class cThreadRefreshDisplay;

class cMain : public wxFrame
{
public: //constructor
    cMain(cEntry*);

private: //event handlers
    void OnMenuClickedConstants(wxCommandEvent& evt);
    void OnMenuClickedInformation(wxCommandEvent& evt);
    void OnMenuClickedExit(wxCommandEvent& evt);
    void OnModbusUpdate(wxCommandEvent& evt);
    void OnClockUpdate(wxCommandEvent& evt);
    void OnRefreshDisplay(wxCommandEvent& evt);
    void OnButtonClickedZ1Open(wxCommandEvent& evt);
    void OnButtonClickedZ1Close(wxCommandEvent& evt);
    void OnButtonClickedZ2Open(wxCommandEvent& evt);
    void OnButtonClickedZ2Close(wxCommandEvent& evt);
    void OnButtonClickedZ3Open(wxCommandEvent& evt);
    void OnButtonClickedZ3Close(wxCommandEvent& evt);
    void OnButtonClickedMotorExtend(wxCommandEvent& evt);
    void OnButtonClickedMotorRetract(wxCommandEvent& evt);
    void OnButtonClickedConfirmRadiation(wxCommandEvent& evt);
    void OnButtonClickedStart(wxCommandEvent& evt);
    void OnButtonClickedStop(wxCommandEvent& evt);
    void OnButtonClickedZeroRadiation(wxCommandEvent& evt);
    void OnButtonClickedClearTimeoutError(wxCommandEvent& evt);
    void OnButtonClickedExperiment(wxCommandEvent& evt);
    void OnTimeoutStart(wxTimerEvent& evt);
    void OnTimeoutStop(wxTimerEvent& evt);
    void OnTimeoutMotor(wxTimerEvent& evt);
    void OnZ1OpenToggle(wxTimerEvent& evt);
    void OnZ1CloseToggle(wxTimerEvent& evt);
    void OnZ2Toggle(wxTimerEvent& evt);
    void OnZ3Toggle(wxTimerEvent& evt);
    void OnClose(wxCloseEvent& evt);
    void OnModbusWriteError(wxTimerEvent& evt);

private: //other methods
    void ClearTimeoutError(void);
    void RefreshDisplay(void);
    void TerminateApp(void);

public: //menu and information window
    cEntry* pEntry;
    cInformation* pInformation;
    cConstants* pConstants;
    wxMenu* mHelpMenu;
    bool informationWindowIsOpen;
    bool constantsWindowIsOpen;

public: //other variables
    cModbus* mbMaster;
	bool bAwaitingReply;

private: //menu
    wxMenuBar* mMenuBar;
    wxMenu* mOptionsMenu;
    wxMenu* mTime;

private: //timers and stopwatch
    wxTimer* timerStart;
    wxTimer* timerStop;
    wxTimer* timerMotor;
    wxTimer* timerZ1OpenToggle;
    wxTimer* timerZ1CloseToggle;
    wxTimer* timerZ2Toggle;
    wxTimer* timerZ3Toggle;
    wxTimer* timerModbusWriteError;
    wxStopWatch stopwatch;

private: //UI elements

    //FONTS
    wxFont myFont;

    //PANELS
    wxPanel* panDiagrams;
	wxPanel* panValves;
	wxPanel* panMotors;
	wxPanel* panMode;
	wxPanel* panRadiationControl;
	wxPanel* panRadiationResults;
	wxPanel* panRadiationTime;
	wxPanel* panStartStop;
	wxPanel* panCurrent;
	wxPanel* panMeasurements;

	//DIAGRAMS
    wxStaticBitmap* imgBackground;
    wxStaticBitmap* imgWater;
    wxStaticBitmap* imgHeadExtended;
    wxStaticBitmap* imgHeadRetracted;

	//INFO TEXT
    wxStaticText* Z2Info;
    wxStaticText* Z3Info;
    wxStaticText* Z4Info;

    //VALVE STATUS TEXT
    wxStaticText* txtZ1Status;
    wxStaticText* txtZ2Status;
    wxStaticText* txtZ3Status;
	wxStaticText* txtZ4Status;

	//MOTOR SCREWED STATUS
	wxStaticText* txtMotorScrewedTitle;
	wxStaticText* txtMotorScrewedStatus;

	//VALVE CONTROL
    wxButton* btnOpenZ1;
    wxButton* btnCloseZ1;
    wxButton* btnOpenZ2;
    wxButton* btnCloseZ2;
    wxButton* btnOpenZ3;
    wxButton* btnCloseZ3;
    wxStaticText* txtValveZ1;
    wxStaticText* txtValveZ2;
    wxStaticText* txtValveZ3;

    //MOTOR CONTROL
    wxButton* btnExtendMotor;
    wxButton* btnRetractMotor;
    wxStaticText* txtMotor;
    wxStaticText* txtSystemTime;

    //RADIATION CONTROL
    wxTextCtrl* txtCtrlRadiation;
    wxButton* btnConfirmRadiation;
    wxStaticText* txtTitleRadiation;
    wxStaticText* txtUnitsRadiation;

    //START/STOP CONTROL
	wxButton* btnExperiment;
    wxButton* btnStart;
    wxButton* btnStop;

    //RADIATION RESULTS
    wxStaticText* txtTitleTimeElapsed;
    wxStaticText* txtTitleCurrentRadiation;
    wxStaticText* txtTitleTimeToCompletion;
    wxStaticText* txtBoxTimeElapsed;
    wxStaticText* txtBoxCurrentRadiation;
    wxStaticText* txtBoxTimeToCompletion;
    wxButton* btnZero;

    //MEASUREMENTS
    wxStaticText* txtTitleTemperature;
    wxStaticText* txtBoxTemperature;
    wxStaticText* txtTitlePressure;
    wxStaticText* txtBoxPressure;
    wxStaticText* txtBoxPressureZ1;
    wxStaticText* txtTitleCurrent;
    wxStaticText* txtBoxCurrent;

    //USER MESSAGE TEMPLATES
    wxString msgUserMessage;
    wxString msgTimeoutMessage;
    wxString msgConnectionErrorPLC;
    wxString msgConnectionErrorTPG362;
    wxString msgSendingError;
	wxString msgErrorPLC;
	wxString msgErrorMotor;
	wxString msgErrorPLCNotReady;
	wxString msgErrorPLCNoStop;
	wxString msgRadiationActive;
    wxString msgLocalMode;
	wxString msgReady;

    //USER MESSAGES
    wxStaticText* txtUserMode;
    wxStaticText* txtUserMessage;
    wxStaticText* txtSendingError;
    wxButton* btnClearTimeoutError;

public: //variables

	float Kp;
	float lambda;

private: //variables

    //ACTIVITY CALCULATIONS
    float Np_target;
    float Np;
    float Np_tminus;
    float delta_Np;
	float A_target;
    float A;
    float P1;
    float P2;
    char P1_status;
    char P2_status;
    char* cP1;
    char* cP2;

    //MEASUREMENTS
    wxCriticalSection measurement_guard;
    float I;
	double T;

    //OTHER
    float dt;
    uint32_t timeElapsedMs;
    uint32_t timeElapsedHour;
	uint32_t timeElapsedMin;
	uint32_t timeElapsedSec;
	uint32_t timeToCompletionHour;
	uint32_t timeToCompletionMin;
	uint32_t timeToCompletionRefresh;
	float timeToCompletionSec;
	bool bExperimentReset;
	bool bExperimentIsRunning;
	bool bExperimentStopped;
	bool bWriteOperationComplete;
	bool bTimeoutError;
	bool bRefreshDisplay;
	bool bHeadCommandExtend;
	bool bCommandStartWater;

	//LOCAL STRINGS
    wxString strTimeElapsed;
    wxString strTimeElapsedHour;
    wxString strTimeElapsedMin;
    wxString strTimeElapsedSec;
	wxString strTimeToCompletion;
	wxString strTimeToCompletionHour;
    wxString strTimeToCompletionMin;
    wxString strTimeToCompletionSec;
    wxString strTemperature;

	//MODBUS DATA
	uint8_t mbStatusValves[4] = { 0 };
	uint8_t mbStatusWater;
	uint8_t mbStatusHeadRetracted;
	uint8_t mbStatusHeadExtended;
	uint8_t mbStatusMotorScrewed;
	uint8_t mbStatusMotorUnscrewed;
	uint8_t mbStatusAlert;
	uint8_t mbStatusLocalMode;
	uint8_t mbCoilRetract;
	uint8_t mbCoilExtend;
	uint8_t mbErrors[4] = { 0 };
	uint16_t mbReadingTemperature;
	char* mbReadingPressure;
	uint16_t mbReadingCurrent;
    int32_t rc;

    //TPG362 STATUS
	bool bStatusTPG362;

    //TCP/IP DATA
    bool bTCPActive;
    wxCriticalSection tpg362_guard;
	struct sockaddr_in tpg362_Address;
    int32_t tpg362_Socket;

    //THREADS
    cThreadTPG362* threadTPG362 = nullptr;
    cThreadSystemClock* threadSystemClock = nullptr;
    cThreadRefreshDisplay* threadRefreshDisplay = nullptr;

    //SYSTEM CLOCK
    friend class cThreadSystemClock;
    friend class cThreadTPG362;
    friend class cThreadRefreshDisplay;

    wxDECLARE_EVENT_TABLE();
};


