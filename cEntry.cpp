#include "cEntry.h"

wxIMPLEMENT_APP(cEntry);

cEntry::cEntry()
{
	//Initialize variables and pointers
    pMain = nullptr;
	mbMaster = nullptr;
	pResetCoils = nullptr;
    wxChecker = nullptr;
    mbAddress = 0;
    tpg362_Socket = 0;
    bTCPActive = false;
    bLogOpen = false;
	Kp = 0;
	lambda = 0;
}

bool cEntry::OnInit()
{
    wxApp::SetAppName(wxString(_T("Naświetlanie Wiązką Wewnętrzną")));

    {
        wxLogNull logNo; //suppress information message
        wxChecker = new wxSingleInstanceChecker(_T(".naswietlaniewiazka_temp"));
    }

    //Check if process already exists
    if (wxChecker->IsAnotherRunning())
    {
        wxLogError(_T("\n       Proces już istnieje!       "));
        delete wxChecker;
        wxChecker = nullptr;
        return false;
    }

	//Open configuration file
	filename = wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/NaswietlanieWiazka/konfiguracja.txt";
	tConfigFile.Open(filename);

	while (!tConfigFile.Eof())
	{
		strConfig = tConfigFile.GetNextLine();

		if ((strConfig[0] != '#') && !(strConfig.IsEmpty()))
		{
			if (strConfig.Mid(0, 12) == wxString("Adres Modbus"))
			{
				temp = strConfig.Mid(14);
				mbAddress = wxAtoi(temp);
			}
			else if (strConfig.Mid(0, 14) == wxString("Port Szeregowy"))
			{
			    mbPort = strConfig.Mid(16, 20);
			}
			else if (strConfig.Mid(0, 12) == wxString("TCP (T/N): T"))
			{
				bTCPActive = true;
			}
			else if (strConfig.Mid(0, 2) == wxString("IP"))
			{
				tpg362_IP = strConfig.Mid(4, 15);
			}
			else if (strConfig.Mid(0, 4) == wxString("Port"))
			{
				tpg362_Port = strConfig.Mid(6, 5);
			}
		}
	}

	//Get system date and time
	now = wxDateTime::Now();

	if (now.GetDay() < 10) time = wxString::Format("0%d.", now.GetDay());
	else time = wxString::Format("%d.", now.GetDay());

	if (now.GetMonth() < 10) time += wxString::Format("0%d.", now.GetMonth());
	else time += wxString::Format("%d.", now.GetMonth());

	if (now.GetYear() < 10) time += wxString::Format("0%d ", now.GetYear());
	else time += wxString::Format("%d ", now.GetYear());

	if (now.GetHour() < 10) time += wxString::Format("0%d:", now.GetHour());
	else time += wxString::Format("%d:", now.GetHour());

	if (now.GetMinute() < 10) time += wxString::Format("0%d:", now.GetMinute());
	else time += wxString::Format("%d:", now.GetMinute());

	if (now.GetSecond() < 10) time += wxString::Format("0%d", now.GetSecond());
	else time += wxString::Format("%d", now.GetSecond());

	//Create log file
	tLogFileName = time + wxString(".txt");
	tLogFile.Create(wxString("/dev/shm/") + tLogFileName);
	tLogFile.AddLine(time);
	tLogFile.AddLine(wxString(""));
	wxString constants = _T("Stała Produkcji = ") + wxString::Format("%.2E, ", Kp) + _T("Stała Rozpadu = ") + wxString::Format("%.2E", lambda);
	tLogFile.AddLine(constants);
	tLogFile.AddLine(wxString(""));
	tLogFile.AddLine(wxString(_T("  Czas    Prąd (nA)")));
	tLogFile.AddLine(wxString(""));
	tLogFile.Write();

	//Initiate TCP/IP connection with TPG362
	tpg362_Socket = socket(AF_INET, SOCK_STREAM, 0);
	tpg362_Address.sin_family = AF_INET;
	tpg362_Address.sin_port = htons(wxAtoi(tpg362_Port));
	inet_pton(AF_INET, tpg362_IP.c_str(), &tpg362_Address.sin_addr);

    //Create Main window
    pMain = new cMain(this);
	pMain->SetIcon(wxICON(icon_small));

	//Start Modbus master
	mbMaster = new cModbus(this, pMain);
	while (!mbMaster->bStartMain) {};

	//Show Main or Reset Coils window
	if (!mbMaster->bErrors && mbMaster->sumCoils > 0)
	{
		pResetCoils = new cResetCoils(pMain, mbMaster);
		pResetCoils->Show();
	}
	else
	{
		wxThread::Sleep(1000);
		pMain->Show();
	}

	return true;
}

int cEntry::OnExit()
{
    //Close Modbus gateway and free resources
    mbMaster->CloseModbus();

    //Free pointers
    delete mbMaster;
    delete wxChecker;
    mbMaster = nullptr;
    wxChecker = nullptr;

    return 0;
}
