#include "cMain.h"

/* Start timeout event */
void cMain::OnTimeoutStart(wxTimerEvent& evt)
{
	//Set flags
	bTimeoutError = true;
	bAwaitingReply = false;
	msgTimeoutMessage = msgErrorPLCNotReady;
	mbMaster->ModbusWriteCoil(COIL_START, 0);
}

void cMain::OnTimeoutStop(wxTimerEvent& evt)
{
	//Set flag
	bTimeoutError = true;
	bAwaitingReply = false;
	msgTimeoutMessage = msgErrorPLCNoStop;
	mbMaster->ModbusWriteCoil(COIL_STOP, 0);
}

/* Motor timeout event */
void cMain::OnTimeoutMotor(wxTimerEvent& evt)
{
	//Set flags
	bTimeoutError = true;
	bWriteOperationComplete = true;
	msgTimeoutMessage = msgErrorMotor;

	{
		wxCriticalSectionLocker enter(mbMaster->mb_guard);
		bHeadCommandExtend = (bool) mbMaster->mb_mapping->tab_bits[COIL_EXTEND_HEAD - 1];
	}

	if (bHeadCommandExtend) mbMaster->ModbusWriteCoil(COIL_EXTEND_HEAD, 0);
	else mbMaster->ModbusWriteCoil(COIL_RETRACT_HEAD, 0);
}

/* Z1 Open Valve Toggle event */
void cMain::OnZ1OpenToggle(wxTimerEvent& evt)
{
	mbMaster->ModbusWriteCoil(COIL_OPEN_Z1, 0);
	bWriteOperationComplete = true;
}

/* Z1 Close Valve Toggle event */
void cMain::OnZ1CloseToggle(wxTimerEvent& evt)
{
	mbMaster->ModbusWriteCoil(COIL_CLOSE_Z1, 0);
	bWriteOperationComplete = true;
}

/* Z2 Valve Toggle event */
void cMain::OnZ2Toggle(wxTimerEvent& evt)
{
	mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z2, 0);
	bWriteOperationComplete = true;
}

/* Z3 Valve Toggle event */
void cMain::OnZ3Toggle(wxTimerEvent& evt)
{
	mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z3, 0);
	bWriteOperationComplete = true;
}

/* Modbus Write Error */
void cMain::OnModbusWriteError(wxTimerEvent& evt)
{
	txtSendingError->Hide();
	if (!txtUserMessage->IsShown()) txtUserMessage->Show();
}
