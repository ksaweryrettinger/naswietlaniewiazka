#pragma once

#include "modbus.h"
#include <unistd.h>
#include <algorithm>
#include "wx/wx.h"
#include "cMain.h"
#include "cEntry.h"
#include "constants.h"

using namespace std;

class cThreadRTU;
class cThreadWriteRTU;
class cEntry;
class cMain;

DECLARE_EVENT_TYPE(wxEVT_RTU_UPDATE, -1)

/* Modbus Gateway class */
class cModbus : wxEvtHandler
{
public:
    cModbus(cEntry*, cMain*);
    void CloseModbus(void);
    void ModbusWriteCoil(int32_t, int32_t);

public: //main frame pointer
    cMain* pMain;
    cEntry* pEntry;

public: //modbus data mapping and data protection
    modbus_t* ctx;
    modbus_mapping_t* mb_mapping;
    wxCriticalSection mb_guard;

public:
    uint8_t sumCoils;
    bool bErrors;
    bool bStartMain;
    bool bReplyReceived;

private:
    bool bCommandSent;

protected: //multithreading
    cThreadRTU* mbThreadRTU;
    cThreadWriteRTU* mbThreadWriteRTU;
    friend class cThreadRTU;
    friend class cThreadWriteRTU;
};

/* Modbus RTU thread class */
class cThreadRTU : public wxThread
{
public:
    cThreadRTU(cModbus*);
    ~cThreadRTU();

protected:
    virtual ExitCode Entry();
    cModbus* mbMaster;

private:
    int32_t rc[3];
    int32_t ct;
};

/* Modbus RTU Write class */
class cThreadWriteRTU : public wxThread
{
public:
	cThreadWriteRTU(cModbus*, int32_t, int32_t);

protected:
    virtual ExitCode Entry();
    cModbus* mbMaster;

private:
    int32_t rc;
    int32_t address;
    int32_t status;
};
