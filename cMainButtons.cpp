#include "cMain.h"

/* Button clicked event */
void cMain::OnButtonClickedZ1Open(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_OPEN_Z1, 1);
		timerZ1OpenToggle->Start(Z1_OPEN_TOGGLE_TIMEOUT, true);
	}

    evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedZ1Close(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_CLOSE_Z1, 1);
		timerZ1CloseToggle->Start(Z1_CLOSE_TOGGLE_TIMEOUT, true);
	}

    evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedZ2Open(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z2, 1);
		timerZ2Toggle->Start(Z2_TOGGLE_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedZ2Close(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z2, 1);
		timerZ2Toggle->Start(Z2_TOGGLE_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedZ3Open(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z3, 1);
		timerZ3Toggle->Start(Z3_TOGGLE_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedZ3Close(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_TOGGLE_Z3, 1);
		timerZ3Toggle->Start(Z3_TOGGLE_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedMotorExtend(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_EXTEND_HEAD, 1);
		if (timerMotor->IsRunning()) timerMotor->Stop();
		timerMotor->Start(MOTOR_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedMotorRetract(wxCommandEvent& evt)
{
	if (!bAwaitingReply)
	{
		bWriteOperationComplete = false;
		bAwaitingReply = true;
		mbMaster->ModbusWriteCoil(COIL_RETRACT_HEAD, 1);
		if (timerMotor->IsRunning()) timerMotor->Stop();
		timerMotor->Start(MOTOR_TIMEOUT, true);
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedConfirmRadiation(wxCommandEvent& evt)
{
	//Get new value
	wxString strActivitySetting = txtCtrlRadiation->GetValue();
	A_target = wxAtof(strActivitySetting);

	//Update display
	if (A_target < RADIATION_LIMIT_LOWER) A_target = RADIATION_LIMIT_LOWER;
	else if (A_target > RADIATION_LIMIT_UPPER) A_target = RADIATION_LIMIT_UPPER;
	txtCtrlRadiation->SetValue(wxString::Format(wxT("%.5G"), A_target));

    evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedStart(wxCommandEvent& evt)
{
	int32_t rc;

	if (!bAwaitingReply)
	{

		bAwaitingReply = true;
		btnStart->Disable();

		{
			wxCriticalSectionLocker enter(mbMaster->mb_guard);

			//Write to Modbus coil
			if (mbStatusWater)
			{
				bCommandStartWater = false;
				rc = modbus_write_bit(mbMaster->ctx, COIL_START, 0); //stop water
			}
			else
			{
				bCommandStartWater = true;
				rc = modbus_write_bit(mbMaster->ctx, COIL_START, 1); //start water
			}
			mbMaster->mb_mapping->tab_input_bits[15] = (rc != -1) ? 0 : 1;
		}

		if (rc != -1)
		{
			timerStart->Start(START_TIMEOUT, true);
		}
		else
		{
			bAwaitingReply = false;
			btnStart->Enable();
		}
	}

	evt.Skip();
}

/* Button clicked event */
void cMain::OnButtonClickedStop(wxCommandEvent& evt)
{
	int32_t rc[2];

	if (!bAwaitingReply)
	{
		bAwaitingReply = true;
		btnStop->Disable();

		{
			wxCriticalSectionLocker enter(mbMaster->mb_guard);

			//Write to Modbus coils
			rc[0] = modbus_write_bit(mbMaster->ctx, COIL_START, 0);
			rc[1] = modbus_write_bit(mbMaster->ctx, COIL_STOP, 1);
			if ((rc[0] != -1) && (rc[1] != -1)) mbMaster->mb_mapping->tab_input_bits[15] = 0;
			else mbMaster->mb_mapping->tab_input_bits[15] = 1;
		}

		if ((rc[0] != -1) && (rc[1] != -1))
		{
			timerStop->Start(STOP_TIMEOUT, true);
			bExperimentStopped = true;
		}
		else
		{
			bAwaitingReply = false;
			btnStop->Enable();
		}
	}

	evt.Skip();
}

void cMain::OnButtonClickedExperiment(wxCommandEvent& evt)
{
	if (!bExperimentIsRunning)
	{
		if (Kp > 0 && lambda > 0)
		{
			//Configure stopwatch
			if (bExperimentReset)
			{
				stopwatch.Start(); //restart stopwatch
				bExperimentReset = false;
			}
			else stopwatch.Resume(); //continue experiment

			//Set flags
			bExperimentIsRunning = true;
		}
		else
		{
	        wxLogWarning(_T("\nBrak Parametrów Produkcji\n(Opcje->Parametry Produkcji)"));
		}
	}
	else
	{
		bExperimentIsRunning = false;
		stopwatch.Pause();
	}
}

/* Button clicked event */
void cMain::OnButtonClickedZeroRadiation(wxCommandEvent& evt)
{
	if (!bExperimentIsRunning) //clear all variables
	{
		A = 0;
		Np = 0;
		Np_tminus = 0;
		timeElapsedMs = 0;
		timeToCompletionRefresh = 0;
		strTimeElapsed = wxString("00:00:00");
		strTimeToCompletion = wxString("00:00:00");
		bExperimentReset = true;
	}

    evt.Skip();
}

void cMain::OnButtonClickedClearTimeoutError(wxCommandEvent& evt)
{
	//Hide timeout error and button
	ClearTimeoutError();
	if (!btnStop->IsEnabled() && bExperimentIsRunning) btnStop->Enable();
}
