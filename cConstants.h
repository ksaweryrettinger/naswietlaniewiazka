#pragma once

#include "wx/wx.h"
#include "wx/spinctrl.h"
#include "cMain.h"

class cMain;

class cConstants : public wxFrame
{
public: //constructor
	cConstants(cMain*);

private: //event handlers
	void OnClose(wxCloseEvent& evt);

private: //pointer received in contructor
	cMain* pMain;
	void OnConstantsChanged(wxCommandEvent& evt);

private: //variables and pointers
    wxFont myFont;
    wxTextCtrl* txtCtrlProductionConstant;
    wxStaticText* txtTitleProductionConstant;
	wxStaticText* txtUnitsProductionConstant;
    wxTextCtrl* txtCtrlDecayConstant;
    wxStaticText* txtTitleDecayConstant;
	wxStaticText* txtUnitsDecayConstant;
	wxButton* btnConfirmConstants;

	wxDECLARE_EVENT_TABLE();
};

