#ifndef CTHREADDISPLAYREFRESH_H_
#define CTHREADDISPLAYREFRESH_H_

#include "cMain.h"

DECLARE_EVENT_TYPE(wxEVT_DISPLAY_UPDATE, -1)

/* Display refresh thread */
class cThreadRefreshDisplay : public wxThread
{
public:
	cThreadRefreshDisplay(cMain*);
	~cThreadRefreshDisplay();

protected:
    virtual ExitCode Entry();

private:
    cMain* pMain;
};

#endif
