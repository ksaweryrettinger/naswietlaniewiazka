#include "cModbus.h"

DEFINE_EVENT_TYPE(wxEVT_RTU_UPDATE)

/* MODBUS MAPPING:
 *
 * COILS
 *
 * 1 = Open Valve Z1
 * 2 = Close Valve Z1
 * 3 = Toggle Valve Z2
 * 4 = Toggle Valve Z3
 * 5 = Extend Motor
 * 6 = Retract Motor
 * 7 = Start Bit
 * 8 = Stop Bit
 *
 * INPUT BITS
 *
 * 1-4 = Valve Status
 * 5 = Working Status
 * 6 = Head Retracted Status
 * 7 = Motor Screwed Status
 * 8 = Motor Unscrewed Status
 * 9 = System Failure
 * 10 = Local Mode Status (1 = Local Mode)
 * 11 = TPG362 Connection OK
 * 12 = Head Extended Status
 * 13 = Modbus Read Coils Error
 * 14 = Modbus Read Inputs Error
 * 15 = Modbus Read Input Registers Error
 * 16 = Modbus Write Coils Error
 *
 * INPUT REGISTERS
 *
 * 1 = Water Temperature (T)
 * 2 = Pressure (P)
 * 3 = Current (I)
 *
 */

cModbus::cModbus(cEntry* pEntry, cMain* pMain)
{
    this->pMain = pMain;
    this->pEntry = pEntry;
    ctx = nullptr;
    sumCoils = 0;
    bErrors = true;
    bStartMain = false;
    bCommandSent = false;
    bReplyReceived = false;

    mb_mapping = modbus_mapping_new_start_address(1, MB_NUM_COILS, 10001, MB_NUM_INPUT_BITS + 4, 40001, MB_NUM_REGISTERS, 30001, MB_NUM_INPUT_REGISTERS);

    //Start Modbus RTU thread
    mbThreadRTU = new cThreadRTU(this);
    mbThreadWriteRTU = nullptr;

    if (mbThreadRTU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus RTU thread!");
        delete mbThreadRTU;
        mbThreadRTU = nullptr;
    }

	pMain->mbMaster = this;
}

void cModbus::CloseModbus(void)
{
    {
        wxCriticalSectionLocker enter(mb_guard);

        //Delete RTU thread
        if (mbThreadRTU)
        {
            if (mbThreadRTU->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete Modbus RTU thread!");
        }
    }

    while (1)
    {
        {
            wxCriticalSectionLocker enter(mb_guard);
            if (!mbThreadRTU) break;
        }
        wxThread::This()->Sleep(1);
    }
}

void cModbus::ModbusWriteCoil(int32_t address, int32_t status)
{
	bReplyReceived = false; //clear flag
	mbThreadWriteRTU = new cThreadWriteRTU(this, address, status); //create new RTU write thread

    if (mbThreadWriteRTU->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create Modbus RTU Write thread!");
        delete mbThreadWriteRTU;
        mbThreadWriteRTU = nullptr;
    }
}

/*********************************** RTU Thread ******************************************/

cThreadRTU::cThreadRTU(cModbus *mbMaster) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    mbMaster->sumCoils = 0;
    rc[0] = -1;
    rc[1] = -1;
    rc[2] = -1;

	//Create new RTU configuration
    mbMaster->ctx = modbus_new_rtu(mbMaster->pEntry->mbPort.mb_str(), MB_BAUD, 'E', 8, 1);
	modbus_set_slave(mbMaster->ctx, mbMaster->pEntry->mbAddress);
	modbus_rtu_set_serial_mode(mbMaster->ctx, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbMaster->ctx, MB_RESPONSE_TIMEOUT_SEC, MB_RESPONSE_TIMEOUT_US);
	ct = modbus_connect(mbMaster->ctx);
}

cThreadRTU::~cThreadRTU()
{
	wxCriticalSectionLocker enter(mbMaster->mb_guard);
    modbus_close(mbMaster->ctx);
	modbus_free(mbMaster->ctx);
    mbMaster->mbThreadRTU = NULL;
}

wxThread::ExitCode cThreadRTU::Entry()
{
    while (!TestDestroy())
    {
        if (ct != -1)
        {
        	wxCriticalSectionLocker enter(mbMaster->mb_guard);

    		if (mbMaster->pMain != nullptr)
    		{
        		if (mbMaster->pMain->bAwaitingReply && mbMaster->bCommandSent)
    			{
    				mbMaster->bReplyReceived = true;
    				mbMaster->bCommandSent = false;
    			}
    		}

    		rc[0] = modbus_read_bits(mbMaster->ctx, 1, MB_NUM_COILS, mbMaster->mb_mapping->tab_bits);
        	rc[1] = modbus_read_input_bits(mbMaster->ctx, 10001, MB_NUM_INPUT_BITS, mbMaster->mb_mapping->tab_input_bits);
        	rc[2] = modbus_read_input_registers(mbMaster->ctx, 30001, MB_NUM_INPUT_REGISTERS, mbMaster->mb_mapping->tab_input_registers);

        	mbMaster->mb_mapping->tab_input_bits[12] = (rc[0] == -1) ? 1 : 0;
        	mbMaster->mb_mapping->tab_input_bits[13] = (rc[1] == -1) ? 1 : 0;
        	mbMaster->mb_mapping->tab_input_bits[14] = (rc[2] == -1) ? 1 : 0;
        }
        else //attempt reconnection
        {
        	mbMaster->mb_mapping->tab_input_bits[12] = (rc[0] == -1) ? 1 : 0;
        	mbMaster->mb_mapping->tab_input_bits[13] = (rc[1] == -1) ? 1 : 0;
        	mbMaster->mb_mapping->tab_input_bits[14] = (rc[2] == -1) ? 1 : 0;
        }

		if (mbMaster->pMain != nullptr)
		{
			//Send event to main window
			wxCommandEvent* evt = new wxCommandEvent(wxEVT_RTU_UPDATE, wxID_ANY);
			((wxEvtHandler*) mbMaster->pMain)->QueueEvent(evt);
		}

		if (!mbMaster->bStartMain)
		{
			if (ct != -1 && rc[0] != -1 && rc[1] != -1 && rc[2] != -1)
			{
				for (int i = 0; i < MB_NUM_COILS; i++) mbMaster->sumCoils += mbMaster->mb_mapping->tab_bits[i];
				mbMaster->bErrors = false;
			}

			mbMaster->bStartMain = true;
		}

        wxThread::Sleep(MB_SCANRATE_MS);
    }

    return (wxThread::ExitCode) 0;
}

/*********************************** RTU Write Thread ******************************************/

cThreadWriteRTU::cThreadWriteRTU(cModbus *mbMaster, int32_t address, int32_t status) : wxThread(wxTHREAD_DETACHED)
{
    this->mbMaster = mbMaster;
    this->address = address;
    this->status = status;
	rc = -1;
}

wxThread::ExitCode cThreadWriteRTU::Entry()
{
	wxCriticalSectionLocker enter(mbMaster->mb_guard);

	//Write to Modbus coil
	int32_t rc = modbus_write_bit(mbMaster->ctx, address, status);
	mbMaster->mb_mapping->tab_input_bits[15] = (rc == -1) ? 1 : 0;
	mbMaster->bCommandSent = true; //set flag

	return (wxThread::ExitCode) 0;
}
