#include "cThreadTPG362.h"

cThreadTPG362::cThreadTPG362(cMain* pMain) : wxThread(wxTHREAD_DETACHED)
{
	this->pMain = pMain;
	this->tpg362_Socket = pMain->tpg362_Socket;
	this->tpg362_Address = pMain->tpg362_Address;
	rdStatus = -1;
	ctStatus = -1;
}

cThreadTPG362::~cThreadTPG362()
{
	//Terminate TCP/IP connection
	close(tpg362_Socket);
	pMain->threadTPG362 = NULL;
}

wxThread::ExitCode cThreadTPG362::Entry()
{
	//Enable socket timeout
	struct timeval tv;
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	setsockopt(tpg362_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

	//Initiate TCP/IP connection
	ctStatus = connect(tpg362_Socket, (struct sockaddr*) &tpg362_Address, sizeof(tpg362_Address));

    while (!TestDestroy())
    {
		//Read pressure
    	rdStatus = read(tpg362_Socket, tpg362_Buffer, 32);

		if (pMain != nullptr && rdStatus != -1)
		{
			wxCriticalSectionLocker enter(pMain->tpg362_guard);

			pMain->P1_status = tpg362_Buffer[0];
			pMain->P2_status = tpg362_Buffer[14];

			//P1 Pressure Gauge
			if (pMain->P1_status != '3' && pMain->P1_status != '4' &&
					pMain->P1_status != '5' && pMain->P1_status != '6')
			{
				// pMain->P1 = wxAtof(wxString(tpg362_Buffer).Mid(2, 11));

				memcpy(pMain->cP1, &tpg362_Buffer[2], 11);
				pMain-> cP1[12] = '\0';
				pMain->P1 = atof(pMain->cP1);
			}

			//P2 Pressure Gauge
			if (pMain->P2_status != '3' && pMain->P2_status != '4' &&
					pMain->P2_status != '5' && pMain->P2_status != '6')
			{
				//pMain->P2 = wxAtof(wxString(tpg362_Buffer).Mid(16, 11));

				memcpy(pMain->cP2, &tpg362_Buffer[16], 11);
				pMain-> cP2[12] = '\0';
				pMain->P2 = atof(pMain->cP2);
			}

			if (!pMain->bStatusTPG362) pMain->bStatusTPG362 = true;
		}
		else if (rdStatus == -1)
		{
			wxCriticalSectionLocker enter(pMain->tpg362_guard);
			pMain->bStatusTPG362 = false;

			//Attempt new connection
			close(tpg362_Socket);
			tpg362_Socket = socket(AF_INET, SOCK_STREAM, 0);
			setsockopt(tpg362_Socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
			ctStatus = connect(tpg362_Socket, (struct sockaddr*) &tpg362_Address, sizeof(tpg362_Address));
		}

    	wxThread::Sleep(TCP_REFRESH_TIME);
    }

	return (wxThread::ExitCode) 0;
}
