#ifndef CTHREADTPG362_H_
#define CTHREADTPG362_H_

#include "cMain.h"

/* TPG362 TCP thread */
class cThreadTPG362 : public wxThread
{
public:
	cThreadTPG362(cMain*);
	~cThreadTPG362();

protected:
    virtual ExitCode Entry();

private:
    cMain* pMain;
	struct sockaddr_in tpg362_Address;
    int32_t tpg362_Socket;
    uint8_t tpg362_Buffer[32];
    int32_t ctStatus;
    int32_t rdStatus;

	friend class cMain;
};

#endif
