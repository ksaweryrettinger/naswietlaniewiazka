#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "Images/icon_small.xpm"
#include "modbus.h"
#include "wx/wx.h"
#include "wx/log.h"
#include "wx/textfile.h"
#include "wx/snglinst.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "cResetCoils.h"
#include "cMain.h"
#include "cModbus.h"
#include "constants.h"

class cMain;
class cModbus;
class cResetCoils;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cModbus* mbMaster;
    cResetCoils* pResetCoils;
    wxSingleInstanceChecker* wxChecker;

public: //Modbus configuration
    int32_t mbAddress;
    wxString mbPort;

public: //TPG362 TCP/IP configuration
	bool bTCPActive;
    int32_t tpg362_Socket;
	struct sockaddr_in tpg362_Address;

public: //other public
	float Kp;
	float lambda;
	wxString tLogFileName;
    wxTextFile tLogFile;

private: //TPG362 TCP/IP strings
	bool bLogOpen;
    wxString tpg362_IP;
    wxString tpg362_Port;

private: //Configuration file variables
    wxTextFile tConfigFile;
    wxString filename;
    wxString strConfig;

private: //other variables
    wxString temp;
	wxDateTime now;
	wxString time;
};

#pragma GCC diagnostic pop
