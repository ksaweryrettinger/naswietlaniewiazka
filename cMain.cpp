#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_MENU(wxID_PREFERENCES, cMain::OnMenuClickedConstants)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_CLOSE, cMain::OnMenuClickedExit)
    EVT_COMMAND(wxID_ANY, wxEVT_RTU_UPDATE, cMain::OnModbusUpdate)
	EVT_COMMAND(wxID_ANY, wxEVT_CLOCK_UPDATE, cMain::OnClockUpdate)
	EVT_COMMAND(wxID_ANY, wxEVT_DISPLAY_UPDATE, cMain::OnRefreshDisplay)
    EVT_BUTTON(10001, cMain::OnButtonClickedZ1Open)
	EVT_BUTTON(10002, cMain::OnButtonClickedZ1Close)
	EVT_BUTTON(10003, cMain::OnButtonClickedZ2Open)
	EVT_BUTTON(10004, cMain::OnButtonClickedZ2Close)
	EVT_BUTTON(10005, cMain::OnButtonClickedZ3Open)
	EVT_BUTTON(10006, cMain::OnButtonClickedZ3Close)
	EVT_BUTTON(10007, cMain::OnButtonClickedMotorExtend)
	EVT_BUTTON(10008, cMain::OnButtonClickedMotorRetract)
	EVT_BUTTON(10009, cMain::OnButtonClickedConfirmRadiation)
	EVT_BUTTON(10010, cMain::OnButtonClickedStart)
	EVT_BUTTON(10011, cMain::OnButtonClickedStop)
	EVT_BUTTON(10012, cMain::OnButtonClickedZeroRadiation)
	EVT_BUTTON(10013, cMain::OnButtonClickedClearTimeoutError)
	EVT_BUTTON(10080, cMain::OnButtonClickedExperiment)
    EVT_TIMER(10014, cMain::OnTimeoutStart)
	EVT_TIMER(10015, cMain::OnTimeoutStop)
	EVT_TIMER(10016, cMain::OnTimeoutMotor)
	EVT_TIMER(10017, cMain::OnZ1OpenToggle)
	EVT_TIMER(10018, cMain::OnZ1CloseToggle)
	EVT_TIMER(10019, cMain::OnZ2Toggle)
	EVT_TIMER(10020, cMain::OnZ3Toggle)
	EVT_TIMER(10021, cMain::OnModbusWriteError)
    EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(400, 150), wxSize(800, 600), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //Set window background colour
    this->SetBackgroundColour(wxColour(230, 230, 230));

    //Set frame mFrameTitle
    this->SetTitle(_T("Naświetlanie Wiązką Wewnętrzną"));

    mbMaster = nullptr;
    pInformation = nullptr;
    pConstants = nullptr;
    this->pEntry = pEntry;
    bAwaitingReply = false;

    //Other variables initialization
    informationWindowIsOpen = false;
    constantsWindowIsOpen = false;

	//MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mHelpMenu = new wxMenu();
	mOptionsMenu = new wxMenu();
	mTime = new wxMenu();

	if (mHelpMenu != nullptr)
	{
		mHelpMenu->Append(wxID_INFO, _T("&Informacje"));
		mOptionsMenu->Append(wxID_PREFERENCES, _T("&Parametry Produkcji"));
		mOptionsMenu->Append(wxID_CLOSE, _T("&Wyjdź"));
		mMenuBar->Append(mTime, _T("00:00:00"));
		mMenuBar->Append(mOptionsMenu, _T("Opcje"));
		mMenuBar->Append(mHelpMenu, _T("&Pomoc"));
	}

	SetMenuBar(mMenuBar);
	mMenuBar->EnableTop(0, false);

    //TIMERS
    timerStart = new wxTimer(this, 10014);
    timerStop = new wxTimer(this, 10015);
    timerMotor = new wxTimer(this, 10016);
    timerZ1OpenToggle = new wxTimer(this, 10017);
    timerZ1CloseToggle = new wxTimer(this, 10018);
    timerZ2Toggle = new wxTimer(this, 10019);
    timerZ3Toggle = new wxTimer(this, 10020);
    timerModbusWriteError = new wxTimer(this, 10021);

    //PANELS
    panDiagrams = new wxPanel(this, wxID_ANY, wxPoint(10, 10), wxSize(600, 310), wxSUNKEN_BORDER);
    panValves = new wxPanel(this, wxID_ANY, wxPoint(620, 10), wxSize(170, 220), wxSUNKEN_BORDER, wxString(_T("Zawory")));
    panMotors = new wxPanel(this, wxID_ANY, wxPoint(620, 240), wxSize(170, 80), wxSUNKEN_BORDER, wxString(_T("Napęd")));
    panMode = new wxPanel(this, wxID_ANY, wxPoint(10, 330), wxSize(135, 30), wxSUNKEN_BORDER);
    panRadiationControl = new wxPanel(this, wxID_ANY, wxPoint(10, 370), wxSize(280, 75), wxSUNKEN_BORDER);
    panStartStop = new wxPanel(this, wxID_ANY, wxPoint(10, 455), wxSize(280, 80), wxSUNKEN_BORDER);
    panRadiationResults = new wxPanel(this, wxID_ANY, wxPoint(300, 370), wxSize(250, 75), wxSUNKEN_BORDER);
    panRadiationTime = new wxPanel(this, wxID_ANY, wxPoint(300, 455), wxSize(250, 80), wxSUNKEN_BORDER);
    panCurrent = new wxPanel(this, wxID_ANY, wxPoint(560, 370), wxSize(230, 50), wxSUNKEN_BORDER);
    panMeasurements = new wxPanel(this, wxID_ANY, wxPoint(560, 430), wxSize(230, 105), wxSUNKEN_BORDER);

    //DIAGRAMS
	imgBackground = new wxStaticBitmap(panDiagrams, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/NaswietlanieWiazka/Images/Diagram 600x300.png"),
																  wxBITMAP_TYPE_PNG), wxPoint(0, 11), wxSize(600, 300));

	imgWater = new wxStaticBitmap(panDiagrams, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/NaswietlanieWiazka/Images/Water Indicator.png"),
																  wxBITMAP_TYPE_PNG), wxPoint(433, 63), wxSize(200, 200));

	imgHeadExtended = new wxStaticBitmap(panDiagrams, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/NaswietlanieWiazka/Images/Motor Extended Dark.png"),
																  wxBITMAP_TYPE_PNG), wxPoint(-55, -1), wxSize(200, 200));

	imgHeadRetracted = new wxStaticBitmap(panDiagrams, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) + wxString("/eclipse-workspace/NaswietlanieWiazka/Images/Motor Retracted Dark.png"),
																  wxBITMAP_TYPE_PNG), wxPoint(325, 29), wxSize(200, 200));

	//INFO TEXT
	Z2Info = new wxStaticText(panDiagrams, wxID_ANY, _T("Zawór Próżni Wstępnej"), wxPoint(280, 9), wxSize(200, 20), wxALIGN_CENTRE_HORIZONTAL);
	Z4Info = new wxStaticText(panDiagrams, wxID_ANY, _T("Zapowietrzenie"), wxPoint(147, 280), wxSize(200, 20), wxALIGN_CENTRE_HORIZONTAL);
	Z3Info = new wxStaticText(panDiagrams, wxID_ANY, _T("Zawór Zrzutowy"), wxPoint(409, 259), wxSize(200, 20), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    Z2Info->SetFont(myFont);
    Z3Info->SetFont(myFont);
    Z4Info->SetFont(myFont);

	//VALVE STATUS TEXT
	txtZ1Status = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(89, 174), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtZ2Status = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(222, 32), wxSize(120, 20), wxALIGN_RIGHT);
	txtZ3Status = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(448, 279), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtZ4Status = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(95, 241), wxSize(120, 20), wxALIGN_RIGHT);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtZ1Status->SetFont(myFont);
    txtZ2Status->SetFont(myFont);
	txtZ3Status->SetFont(myFont);
	txtZ4Status->SetFont(myFont);
	txtZ1Status->SetForegroundColour(wxColour(220, 0, 0));
	txtZ2Status->SetForegroundColour(wxColour(220, 0, 0));
	txtZ3Status->SetForegroundColour(wxColour(220, 0, 0));
	txtZ4Status->SetForegroundColour(wxColour(220, 0, 0));

	//MOTOR SCREWED STATUS
	txtMotorScrewedTitle = new wxStaticText(panDiagrams, wxID_ANY, _T("Głowica"), wxPoint(472, 27), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
	txtMotorScrewedStatus = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(472, 46), wxSize(120, 20), wxALIGN_CENTRE_HORIZONTAL);
	myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
	txtMotorScrewedStatus->SetFont(myFont);

    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtMotorScrewedTitle->SetFont(myFont);

	imgWater->Hide();
	imgHeadExtended->Hide();
	imgHeadRetracted->Hide();

    //VALVE CONTROL
    btnOpenZ1 = new wxButton(panValves, 10001, _T("Otwórz"), wxPoint(10, 30), wxSize(70, 30));
    btnCloseZ1 = new wxButton(panValves, 10002, _T("Zamknij"),  wxPoint(90, 30), wxSize(70, 30));
    btnOpenZ2 = new wxButton(panValves, 10003, _T("Otwórz"), wxPoint(10, 100), wxSize(70, 30));
    btnCloseZ2 = new wxButton(panValves, 10004, _T("Zamknij"),  wxPoint(90, 100), wxSize(70, 30));
    btnOpenZ3 = new wxButton(panValves, 10005, _T("Otwórz"), wxPoint(10, 170), wxSize(70, 30));
    btnCloseZ3 = new wxButton(panValves, 10006, _T("Zamknij"),  wxPoint(90, 170), wxSize(70, 30));
    btnOpenZ1->SetBackgroundColour(wxColour(220, 220, 220));
    btnCloseZ1->SetBackgroundColour(wxColour(220, 220, 220));
    btnOpenZ2->SetBackgroundColour(wxColour(220, 220, 220));
    btnCloseZ2->SetBackgroundColour(wxColour(220, 220, 220));
    btnOpenZ3->SetBackgroundColour(wxColour(220, 220, 220));
    btnCloseZ3->SetBackgroundColour(wxColour(220, 220, 220));
    btnOpenZ1->Disable();
    btnCloseZ1->Disable();
    btnOpenZ2->Disable();
    btnCloseZ2->Disable();
    btnOpenZ3->Disable();
    btnCloseZ3->Disable();
    txtValveZ1 = new wxStaticText(panValves, wxID_ANY, _T("Zawór Z1"), wxPoint(40, 10), wxSize(90, 10), wxALIGN_CENTRE_HORIZONTAL);
    txtValveZ2 = new wxStaticText(panValves, wxID_ANY, _T("Próżnia Wstępna"), wxPoint(27, 80), wxSize(120, 10), wxALIGN_CENTRE_HORIZONTAL);
    txtValveZ3 = new wxStaticText(panValves, wxID_ANY, _T("Zawór Zrzutowy"), wxPoint(27, 150), wxSize(120, 10), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtValveZ1->SetFont(myFont);
    txtValveZ2->SetFont(myFont);
    txtValveZ3->SetFont(myFont);

    //MOTOR CONTROL
    btnExtendMotor = new wxButton(panMotors, 10007, _T("Wysuń"), wxPoint(10, 30), wxSize(70, 30));
    btnRetractMotor = new wxButton(panMotors, 10008, _T("Schowaj"),  wxPoint(90, 30), wxSize(70, 30));
    btnExtendMotor->SetBackgroundColour(wxColour(220, 220, 220));
    btnRetractMotor->SetBackgroundColour(wxColour(220, 220, 220));
    btnExtendMotor->Disable();
	btnRetractMotor->Disable();
    txtMotor = new wxStaticText(panMotors, wxID_ANY, _T("Głowica"), wxPoint(40, 10), wxSize(90, 10), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtMotor->SetFont(myFont);

    //SYSTEM TIME
    txtSystemTime = new wxStaticText(panMode, wxID_ANY, _T(" "), wxPoint(15, 5), wxSize(100, 10), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtSystemTime->SetFont(myFont);

    //RADIATION CONTROL
    txtCtrlRadiation = new wxTextCtrl(panRadiationControl, wxID_ANY, " ", wxPoint(17, 30), wxSize(120, 30), wxALIGN_CENTRE_HORIZONTAL);
    btnConfirmRadiation = new wxButton(panRadiationControl, 10009, _T("Potwierdź"), wxPoint(178, 30), wxSize(85, 30));
    txtTitleRadiation = new wxStaticText(panRadiationControl, wxID_ANY, _T("Aktywność Zadana"), wxPoint(2, 10), wxSize(150, 10), wxALIGN_CENTRE_HORIZONTAL);
    txtUnitsRadiation = new wxStaticText(panRadiationControl, wxID_ANY, _T("Bq"), wxPoint(142, 36), wxSize(20, 10), wxALIGN_CENTRE_HORIZONTAL);
    btnConfirmRadiation->SetBackgroundColour(wxColour(220, 220, 220));
    myFont = wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtCtrlRadiation->SetFont(myFont);
    txtUnitsRadiation->SetFont(myFont);
    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    txtTitleRadiation->SetFont(myFont);
    txtCtrlRadiation->Disable();
    btnConfirmRadiation->Disable();

    //START/STOP CONTROL
    btnStart = new wxButton(panStartStop, 10010, _T("WODA"), wxPoint(20, 22), wxSize(72, 36));
    btnExperiment = new wxButton(panStartStop, 10080, _T("POMIAR"), wxPoint(104, 22), wxSize(72, 36));
    btnStop = new wxButton(panStartStop, 10011, _T("STOP"), wxPoint(188, 22), wxSize(72, 36));
    btnStart->SetBackgroundColour(wxColour(220, 220, 220));
    btnExperiment->SetBackgroundColour(wxColour(220, 220, 220));
    btnStop->SetBackgroundColour(wxColour(220, 220, 220));
    btnExperiment->Disable();
    btnStart->Disable();
    btnStop->Disable();

    //RADIATION RESULTS
    txtTitleCurrentRadiation = new wxStaticText(panRadiationResults, wxID_ANY, _T("Aktywność Bieżąca"), wxPoint(10, 11), wxSize(150, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtBoxCurrentRadiation = new wxStaticText(panRadiationResults, wxID_ANY, _T(" "), wxPoint(10, 36), wxSize(150, 20), wxALIGN_CENTRE_HORIZONTAL);
    btnZero = new wxButton(panRadiationResults, 10012, _T("Zeruj"), wxPoint(167, 23), wxSize(68, 29));
    btnZero->SetBackgroundColour(wxColour(220, 220, 220));
    myFont = wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
	txtBoxCurrentRadiation->SetFont(myFont);
	txtBoxCurrentRadiation->SetForegroundColour(wxColour(0, 0, 220));

    //RADIATION TIME
    txtTitleTimeElapsed = new wxStaticText(panRadiationTime, wxID_ANY, _T("Czas Bieżący:"), wxPoint(4, 11), wxSize(150, 20), wxALIGN_CENTRE_HORIZONTAL);
    txtBoxTimeElapsed = new wxStaticText(panRadiationTime, wxID_ANY, _T(" "), wxPoint(81, 12), wxSize(150, 17), wxALIGN_RIGHT);
    txtTitleTimeToCompletion = new wxStaticText(panRadiationTime, wxID_ANY, _T("Czas do osiągnięcia\naktywności zadanej:"), wxPoint(4, 36), wxSize(150, 40), wxALIGN_CENTRE_HORIZONTAL);
    txtBoxTimeToCompletion = new wxStaticText(panRadiationTime, wxID_ANY, _T(" "), wxPoint(81, 46), wxSize(150, 20), wxALIGN_RIGHT);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtBoxTimeElapsed->SetFont(myFont);
    txtBoxTimeToCompletion->SetFont(myFont);

    //MEASUREMENTS
	txtTitleCurrent = new wxStaticText(panCurrent, wxID_ANY, _T("Prąd (I):"), wxPoint(20, 15), wxSize(120, 40), wxALIGN_LEFT);
    txtBoxCurrent = new wxStaticText(panCurrent, wxID_ANY, _T(" "), wxPoint(100, 15), wxSize(110, 20), wxALIGN_RIGHT);
    txtTitlePressure = new wxStaticText(panMeasurements, wxID_ANY, _T("Próżnia (P):"), wxPoint(20, 19), wxSize(120, 20), wxALIGN_LEFT);
    txtBoxPressure = new wxStaticText(panMeasurements, wxID_ANY, _T(" "), wxPoint(100, 19), wxSize(110, 20), wxALIGN_RIGHT);
    txtTitleTemperature = new wxStaticText(panMeasurements, wxID_ANY, _T("Temperatura (T):"), wxPoint(20, 65), wxSize(120, 20), wxALIGN_LEFT);
    txtBoxTemperature = new wxStaticText(panMeasurements, wxID_ANY, _T(" "), wxPoint(100, 65), wxSize(110, 20), wxALIGN_RIGHT);
    txtBoxPressureZ1 = new wxStaticText(panDiagrams, wxID_ANY, _T(" "), wxPoint(8, 170), wxSize(60, 50), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtBoxCurrent->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
    txtTitleCurrent->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
    txtBoxTemperature->SetFont(myFont);
    txtBoxPressure->SetFont(myFont);
    myFont = wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtBoxPressureZ1->SetFont(myFont);

	// 1. Brak połączenia z PLC – wszystkie inne błędy i wiadomości nieaktywne.
	// 2. Brak połączenia z próżniomierzem.
	// 3. Błąd transmisji.
	// 4. Lokalne
	// 5. Błąd PLC.
	// 6. Błąd napędu głowicy [Kasuj Błąd]
	// 7. Brak gotowości PLC [Kasuj Błąd]
	// 8. Brak potwierdzenia STOP [Kasuj Błąd]
	// 9. Naświetlanie.
	// 10. Gotowość

    // Naświetlanie. Brak próżniomierza. Błąd PLC. Brak potwierdzenia STOP. [Kasuj Błąd]

    //USER MESSAGES
    msgConnectionErrorPLC = wxString(_T("Brak połączenia z kontrolerem. "));
    msgConnectionErrorTPG362 = wxString(_T("Brak próżniomierza. "));
    msgSendingError = wxString(_T("Błąd transmisji. "));
	msgErrorPLC = wxString(_T("Błąd PLC. "));
	msgErrorMotor = wxString(_T("Błąd napędu głowicy. "));
	msgErrorPLCNotReady = wxString(_T("Brak gotowości PLC. "));
	msgErrorPLCNoStop = wxString(_T("Brak potwierdzenia STOP. "));
	msgRadiationActive = wxString(_T("NAŚWIETLANIE"));
    msgLocalMode = wxString(_T("LOKALNE"));
	msgReady = wxString(_T("GOTOWOŚĆ"));

	//USER MODE
    txtUserMode = new wxStaticText(panMode, wxID_ANY, _T(" "), wxPoint(0, 6), wxSize(135, 30), wxALIGN_CENTRE_HORIZONTAL);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtUserMode->SetFont(myFont);
    txtUserMode->Show();

    //USER MESSAGE
    txtUserMessage = new wxStaticText(this, wxID_ANY, _T(" "), wxPoint(155, 338), wxSize(10, 20), wxALIGN_LEFT);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtUserMessage->SetFont(myFont);
    txtUserMessage->SetForegroundColour(wxColour(220, 0, 0));
    txtUserMessage->Hide();

    //WRITE ERROR MESSAGE
    txtSendingError = new wxStaticText(this, wxID_ANY, msgSendingError, wxPoint(155, 338), wxSize(400, 20), wxALIGN_LEFT);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    txtSendingError->SetFont(myFont);
    txtSendingError->SetForegroundColour(wxColour(220, 0, 0));
    txtSendingError->Hide();

    //TIMEOUT ERROR BUTTON
    btnClearTimeoutError = new wxButton(this, 10013, _T("Kasuj Błąd"), wxPoint(400, 330), wxSize(79, 30));
    btnClearTimeoutError->SetBackgroundColour(wxColour(220, 220, 220));
    btnClearTimeoutError->Disable();
    btnClearTimeoutError->Hide();

    //ACTIVITY CONSTANTS
    Kp = pEntry->Kp;
    lambda = pEntry->lambda;

    //ACTIVITY CALCULATIONS AND OTHER
    Np_target = 0;
    Np = 0;
    Np_tminus = 0;
    delta_Np = 0;
    A_target = 0;
    A = 0;
	P1 = 0;
	P2 = 0;
    P1_status = 0;
    P2_status = 0;
	dt = EXPERIMENT_REFRESH_TIMEOUT / 1000;
	I = 0;
	T = 0;

    //LOCAL VARIABLES
    timeElapsedMs = 0;
    timeElapsedHour = 0;
	timeElapsedMin = 0;
	timeElapsedSec = 0;
	timeToCompletionHour = 0;
	timeToCompletionMin = 0;
	timeToCompletionSec = 0;
	timeToCompletionRefresh = 0;
	bExperimentReset = true;
	bExperimentIsRunning = false;
    bExperimentStopped = false;
    bWriteOperationComplete = true;
    bTimeoutError = false;
    bRefreshDisplay = false;
	bHeadCommandExtend = false;
	bCommandStartWater = false;
	strTimeElapsed = wxString("00:00:00");
	strTimeToCompletion = wxString("00:00:00");

    //MODBUS DATA
	mbStatusWater = 0;
	mbStatusHeadExtended = 0;
	mbStatusHeadRetracted = 0;
	mbStatusMotorScrewed = 0;
	mbStatusMotorUnscrewed = 0;
	mbStatusAlert = 0;
	mbStatusLocalMode = 0;
    mbReadingTemperature = 0;
	mbReadingCurrent = 0;
	mbCoilRetract = 0;
	mbCoilExtend = 0;
	rc = -1;

	mbReadingPressure = new char[27];
	cP1 = new char[11];
	cP2 = new char[11];

	//TPG362 STATUS
	bStatusTPG362 = false;

	//TCP DATA
	bTCPActive = pEntry->bTCPActive;
	tpg362_Socket = pEntry->tpg362_Socket;
	tpg362_Address = pEntry->tpg362_Address;

	if (bTCPActive)
	{
		//Start TPG362 (TCP) thread
		threadTPG362 = new cThreadTPG362(this);

		if (threadTPG362->Run() != wxTHREAD_NO_ERROR)
		{
			wxLogError("Can't create TPG362 thread!");
			delete threadTPG362;
			threadTPG362 = nullptr;
		}
	}

    //Start system clock thread
    threadSystemClock = new cThreadSystemClock(this);

    if (threadSystemClock->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create system clock thread!");
        delete threadSystemClock;
        threadSystemClock = nullptr;
    }

    //Start display refresh thread
    threadRefreshDisplay = new cThreadRefreshDisplay(this);

    if (threadRefreshDisplay->Run() != wxTHREAD_NO_ERROR)
    {
        wxLogError("Can't create display refresh thread!");
        delete threadRefreshDisplay;
        threadRefreshDisplay = nullptr;
    }

    //Initialize experiment results
	txtBoxCurrentRadiation->SetLabel(wxString::Format("%.2f Bq", A));
	txtBoxTimeElapsed->SetLabel(strTimeElapsed);
	txtBoxTimeToCompletion->SetLabel(strTimeToCompletion);
}

/* Modbus RTU event handler */
void cMain::OnModbusUpdate(wxCommandEvent& evt)
{
	/********************************* MODBUS DATA **************************************/

	{
		//Enter critical section
		wxCriticalSectionLocker enter(mbMaster->mb_guard);

		//Valve statuses
		mbStatusValves[0] = !(mbMaster->mb_mapping->tab_input_bits[0]);
		mbStatusValves[1] = !(mbMaster->mb_mapping->tab_input_bits[1]);
		mbStatusValves[2] = !(mbMaster->mb_mapping->tab_input_bits[2]);
		mbStatusValves[3] = !(mbMaster->mb_mapping->tab_input_bits[3]);

		//Other bits
		mbStatusWater = mbMaster->mb_mapping->tab_input_bits[4];
		mbStatusHeadRetracted = mbMaster->mb_mapping->tab_input_bits[5];
		mbStatusHeadExtended = mbMaster->mb_mapping->tab_input_bits[10];
		mbStatusMotorScrewed = mbMaster->mb_mapping->tab_input_bits[6];
		mbStatusMotorUnscrewed = mbMaster->mb_mapping->tab_input_bits[7];
		mbStatusAlert = mbMaster->mb_mapping->tab_input_bits[8];
		mbStatusLocalMode = mbMaster->mb_mapping->tab_input_bits[9];

		//TPG362 connection status
		if (!bTCPActive) bStatusTPG362 = (bool) mbMaster->mb_mapping->tab_input_bits[11];

		//Modbus errors
		mbErrors[0] = mbMaster->mb_mapping->tab_input_bits[12]; //Modbus Read Coils Error
		mbErrors[1] = mbMaster->mb_mapping->tab_input_bits[13]; //Modbus Read Input Bits Error
		mbErrors[2] = mbMaster->mb_mapping->tab_input_bits[14]; //Modbus Read Input Registers Error
		mbErrors[3] = mbMaster->mb_mapping->tab_input_bits[15]; //Modbus Write Error

		//Measurements
		mbReadingTemperature = mbMaster->mb_mapping->tab_input_registers[0];
		mbReadingCurrent = mbMaster->mb_mapping->tab_input_registers[1];

		//Read pressure data via Modbus (TCP/IP disabled)
		if (!bTCPActive && bStatusTPG362)
		{
			//Store pressure string [b,sx.xxxxEsxx,c,sy.yyyyEsyy]
			for (uint8_t i = 0; i < MB_NUM_INPUT_REGISTERS - 3; i++)
			{
				mbReadingPressure[i] = (char) mbMaster->mb_mapping->tab_input_registers[i + 2];
			}

			P1_status = mbReadingPressure[0];
			P2_status = mbReadingPressure[14];

			//P1 Pressure Gauge
			if (P1_status != '3' && P1_status != '4' && P1_status != '5' && P1_status != '6')
			{
				//P1 = wxAtof(wxString(mbReadingPressure).Mid(2, 11));
                memcpy(cP1, &mbReadingPressure[2], 11);
                cP1[12] = '\0';
                P1 = atof(cP1);
			}

			//P2 Pressure Gauge
			if (P2_status != '3' && P2_status != '4' && P2_status != '5' && P2_status != '6')
			{
				//P2 = wxAtof(wxString(mbReadingPressure).Mid(16, 11));
                memcpy(cP2, &mbReadingPressure[16], 11);
                cP2[12] = '\0';
                P2 = atof(cP2);
			}
		}
	}

	/********************************* USER MESSAGES ************************************/

	//Application Mode
	if (mbErrors[0] || mbErrors[1] || mbErrors[2]) //Modbus errors
	{
		txtUserMode->SetLabel(wxString("---"));
		txtUserMode->SetForegroundColour(wxColour(0, 0, 0));
	}
	else if (mbStatusLocalMode) //Local (PLC) Control
	{
		txtUserMode->SetLabel(msgLocalMode);
		txtUserMode->SetForegroundColour(wxColour(255, 127, 0)); //orange
	}
	else if (!bStatusTPG362 || mbStatusAlert || bTimeoutError) //error mode
	{
		txtUserMode->SetLabel(wxString("---"));
		txtUserMode->SetForegroundColour(wxColour(0, 0, 0));
	}
	else if (bExperimentIsRunning)
	{
		txtUserMode->SetLabel(msgRadiationActive);
		txtUserMode->SetForegroundColour(wxColour(0, 200, 0));
	}
	else if (!mbStatusLocalMode) //Application Control
	{
		txtUserMode->SetLabel(msgReady);
		txtUserMode->SetForegroundColour(wxColour(0, 200, 0));
	}

	msgUserMessage = wxString("");

	//Construct user message
	if (mbErrors[0] || mbErrors[1] || mbErrors[2])
	{
		//Hide button and clear error
		if (bTimeoutError) ClearTimeoutError();
		if (btnClearTimeoutError->IsEnabled()) btnClearTimeoutError->Disable();
		if (btnClearTimeoutError->IsShown()) btnClearTimeoutError->Hide();

		if (txtSendingError->IsShown()) txtSendingError->Hide();
		msgUserMessage += msgConnectionErrorPLC; //Modbus connection error
		if (bTCPActive && !bStatusTPG362) msgUserMessage += msgConnectionErrorTPG362; //TGP362 TCP/IP connection error
	}
	else if (!txtSendingError->IsShown())
	{
		if (!bStatusTPG362) //TPG362 connection error
		{
			msgUserMessage += msgConnectionErrorTPG362;
		}

		if (mbStatusAlert) //PLC error
		{
			msgUserMessage += msgErrorPLC;
		}

		if (bTimeoutError) //timeout error
		{
			msgUserMessage += msgTimeoutMessage;
			if (!btnClearTimeoutError->IsEnabled()) btnClearTimeoutError->Enable();
		}
		else
		{
			//Hide button
			if (btnClearTimeoutError->IsEnabled()) btnClearTimeoutError->Disable();
			if (btnClearTimeoutError->IsShown()) btnClearTimeoutError->Hide();
		}
	}

	//Update message
	if (!msgUserMessage.IsEmpty())
	{
		txtUserMessage->SetLabel(msgUserMessage);

		//Ensure width of textbox matches width of message
		wxClientDC dc(txtUserMessage);
		wxSize size = dc.GetTextExtent(msgUserMessage);
		txtUserMessage->SetSize(size.x + 15, txtUserMessage->GetSize().y);

		if (!txtUserMessage->IsShown() && !txtSendingError->IsShown()) txtUserMessage->Show();

		if (bTimeoutError)
		{
			btnClearTimeoutError->SetPosition(wxPoint(151 + txtUserMessage->GetSize().GetWidth(), 330));
			if (!btnClearTimeoutError->IsShown()) btnClearTimeoutError->Show();
		}
	}
	else if (txtUserMessage->IsShown() && msgUserMessage.IsEmpty()) txtUserMessage->Hide();

	/********************************* MODBUS READ ERRORS *******************************/

	if (mbErrors[0] || mbErrors[1] || mbErrors[2])
	{
		//Pause display refresh
		if (bRefreshDisplay) bRefreshDisplay = false;

		//Disable buttons
		if (btnOpenZ1->IsEnabled()) btnOpenZ1->Disable();
		if (btnCloseZ1->IsEnabled()) btnCloseZ1->Disable();
		if (btnOpenZ2->IsEnabled()) btnOpenZ2->Disable();
		if (btnCloseZ2->IsEnabled()) btnCloseZ2->Disable();
		if (btnOpenZ3->IsEnabled()) btnOpenZ3->Disable();
		if (btnCloseZ3->IsEnabled()) btnCloseZ3->Disable();
		if (btnExtendMotor->IsEnabled()) btnExtendMotor->Disable();
		if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
		if (btnStart->IsEnabled()) btnStart->Disable();
		if (btnStop->IsEnabled()) btnStop->Disable();
		if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
		if (btnConfirmRadiation->IsEnabled()) btnConfirmRadiation->Disable();
		if (btnZero->IsEnabled()) btnZero->Disable();
		if (btnExperiment->IsEnabled()) btnExperiment->Disable();
		btnExperiment->SetBackgroundColour(wxColour(220, 220, 220));

		//Disable text input
		txtCtrlRadiation->SetValue(_T("-"));
		if (txtCtrlRadiation->IsEnabled()) txtCtrlRadiation->Disable();

		//Update valve and motor statuses
		txtZ1Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ2Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ3Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ4Status->SetForegroundColour(wxColour(220, 0, 0));
		txtMotorScrewedStatus->SetForegroundColour(wxColour(220, 0, 0));
		txtZ1Status->SetLabel(_T("---"));
		txtZ2Status->SetLabel(_T("---"));
		txtZ3Status->SetLabel(_T("---"));
		txtZ4Status->SetLabel(_T("---"));
		txtMotorScrewedStatus->SetLabel(_T("---"));

		//Update measurements
		txtBoxCurrent->SetLabel(_T("---"));
		txtBoxTemperature->SetLabel(_T("---"));
		txtBoxPressure->SetLabel(_T("---"));
		txtBoxPressureZ1->SetPosition(wxPoint(8, 180));
		txtBoxPressureZ1->SetLabel(_T("---"));

		//Hide diagrams
		if (imgWater->IsShown()) imgWater->Hide();
		if (imgHeadExtended->IsShown()) imgHeadExtended->Hide();
		if (imgHeadRetracted->IsShown()) imgHeadRetracted->Hide();

		return; //MODBUS READ ERRORS -> RETURN FROM EVENT HANDLER
	}

	/********************************* MODBUS WRITE ERROR *******************************/

	if (mbErrors[3])
	{
		//Hide lower priority messages
		if (txtUserMessage->IsShown()) txtUserMessage->Hide();

		{
			//Enter critical section
			wxCriticalSectionLocker enter(mbMaster->mb_guard);
			mbMaster->mb_mapping->tab_input_bits[15] = 0; //clear write error
		}

		//Display error message
		if (!txtSendingError->IsShown()) txtSendingError->Show();
		timerModbusWriteError->Start(MODBUS_WRITE_ERROR_TIMEOUT, true);

		//Clear flag
		if (bAwaitingReply && timerMotor->IsRunning())
		{
			timerMotor->Stop();
			bWriteOperationComplete = true;
		}
	}

	/********************************* MOTOR ERRORS **************************************/

	//Check for motor errors
	if (mbStatusHeadExtended == mbStatusHeadRetracted && !timerMotor->IsRunning()
			&& !bTimeoutError && msgTimeoutMessage != msgErrorMotor && !mbStatusLocalMode)
	{
		timerMotor->Start(MOTOR_TIMEOUT, true);
	}

	/********************************* MOTOR CONFIRMATION ********************************/

	//Check if motor operation is successful
	if (timerMotor->IsRunning() && mbStatusHeadExtended != mbStatusHeadRetracted)
	{
		{
			wxCriticalSectionLocker enter(mbMaster->mb_guard);
			mbCoilRetract = mbMaster->mb_mapping->tab_bits[COIL_RETRACT_HEAD - 1];
			mbCoilExtend = mbMaster->mb_mapping->tab_bits[COIL_EXTEND_HEAD - 1];
		}

		if (mbStatusHeadRetracted && mbCoilRetract)
		{
			wxThread::Sleep(100);
			mbMaster->ModbusWriteCoil(COIL_RETRACT_HEAD, 0);
		}
		else if (mbStatusHeadExtended && mbCoilExtend)
		{
			wxThread::Sleep(100);
			mbMaster->ModbusWriteCoil(COIL_EXTEND_HEAD, 0);
		}

		bWriteOperationComplete = true;
		timerMotor->Stop();
	}

	/********************************* WATER CONFIRMATION ********************************/

	if (timerStart->IsRunning() && (bCommandStartWater == mbStatusWater))
	{
		timerStart->Stop();
		bAwaitingReply = false;
		if (!btnStop->IsEnabled()) btnStop->Enable();  //configure STOP button
	}

	/********************************* EXPERIMENT STOPPED ********************************/

	if (timerStop->IsRunning() && bExperimentStopped && !mbStatusWater)
	{
		timerStop->Stop();
		bAwaitingReply = false;
		bExperimentStopped = false;
		bExperimentIsRunning = false;
	}

	/********************************* CLEAR STOP SIGNAL ********************************/

	if (mbMaster->mb_mapping->tab_bits[COIL_STOP - 1] && mbStatusValves[2]) //Z3 open
	{
		mbMaster->ModbusWriteCoil(COIL_STOP, 0);
	}

	/********************************* BUTTON CONTROL ************************************/

	if (!mbStatusLocalMode) //NORMAL OPERATION
	{
		if (bTimeoutError) //TIMEOUT -> DISABLE ALL BUTTONS
		{
			//Disable all user buttons
			if (btnOpenZ1->IsEnabled()) btnOpenZ1->Disable();
			if (btnCloseZ1->IsEnabled()) btnCloseZ1->Disable();
			if (btnOpenZ2->IsEnabled()) btnOpenZ2->Disable();
			if (btnCloseZ2->IsEnabled()) btnCloseZ2->Disable();
			if (btnOpenZ3->IsEnabled()) btnOpenZ3->Disable();
			if (btnCloseZ3->IsEnabled()) btnCloseZ3->Disable();
			if (btnExtendMotor->IsEnabled()) btnExtendMotor->Disable();
			if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
			if (btnStart->IsEnabled()) btnStart->Disable();
			if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
			if (btnConfirmRadiation->IsEnabled()) btnConfirmRadiation->Disable();
			if (btnZero->IsEnabled()) btnZero->Disable();
			if (btnExperiment->IsEnabled()) btnExperiment->Disable();
			btnExperiment->SetBackgroundColour(wxColour(220, 220, 220));

			//Stop button (special case)
			if (bTimeoutError && btnStop->IsEnabled()) btnStop->Disable();

			//Disable text input
			if (txtCtrlRadiation->IsEnabled()) txtCtrlRadiation->Disable();
			txtCtrlRadiation->SetValue(wxString::Format("%G", A_target));
		}
		else //NORMAL OPERATION
		{
			/********************************* VALVE BUTTONS *************************************/

			//Valve Z1
			if (mbStatusValves[0]) //OPEN
			{
				if (btnOpenZ1->IsEnabled()) btnOpenZ1->Disable();

				if (mbStatusHeadExtended)
				{
					//DO NOT close Z1 if Head is extended
					if (btnCloseZ1->IsEnabled()) btnCloseZ1->Disable();
				}
				else if (!btnCloseZ1->IsEnabled()) btnCloseZ1->Enable();
			}
			else //CLOSED
			{
				if (mbStatusValves[1] || mbStatusValves[2] || mbStatusValves[3] || mbStatusAlert || !mbStatusWater)
				{
					//DO NOT open Z1 if another valve is open or Pressure is too high (system alert) or water is not flowing
					if (btnOpenZ1->IsEnabled()) btnOpenZ1->Disable();
				}
				else if (!btnOpenZ1->IsEnabled()) btnOpenZ1->Enable();

				if (btnCloseZ1->IsEnabled()) btnCloseZ1->Disable();
			}

			//Valve Z2
			if (mbStatusValves[1]) //OPEN
			{
				if (btnOpenZ2->IsEnabled()) btnOpenZ2->Disable();
				if (!btnCloseZ2->IsEnabled()) btnCloseZ2->Enable();
			}
			else //CLOSED
			{
				if (mbStatusValves[0] || mbStatusValves[2] || mbStatusValves[3]) //DO NOT open Z2 if Z1, Z3 or Z4 are open
				{
					if (btnOpenZ2->IsEnabled()) btnOpenZ2->Disable();
				}
				else if (!btnOpenZ2->IsEnabled()) btnOpenZ2->Enable();

				if (btnCloseZ2->IsEnabled()) btnCloseZ2->Disable();
			}

			//Valve Z3
			if (mbStatusValves[2]) //OPEN
			{
				if (btnOpenZ3->IsEnabled()) btnOpenZ3->Disable();
				if (!btnCloseZ3->IsEnabled()) btnCloseZ3->Enable();
			}
			else //CLOSED
			{
				if (mbStatusValves[0] || mbStatusValves[1] || mbStatusValves[3]) //DO NOT open Z3 if Z1, Z2 or Z4 are open
				{
					if (btnOpenZ3->IsEnabled()) btnOpenZ3->Disable();
				}
				else if (!btnOpenZ3->IsEnabled()) btnOpenZ3->Enable();
				if (btnCloseZ3->IsEnabled()) btnCloseZ3->Disable();
			}

			/********************************* MOTOR BUTTONS *************************************/

			if (mbStatusHeadExtended) //EXTENDED
			{
				if (btnExtendMotor->IsEnabled()) btnExtendMotor->Disable();

				if (!mbStatusValves[0]) //Z1 closed
				{
					if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
				}
				else if (!btnRetractMotor->IsEnabled()) btnRetractMotor->Enable();
			}
			else if (mbStatusHeadRetracted) //RETRACTED
			{
				if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();

				if (!mbStatusValves[0]) //Z1 closed
				{
					//DO NOT extend Head if Z1 valve is closed
					if (btnExtendMotor->IsEnabled()) btnExtendMotor->Disable();
				}
				else if (!btnExtendMotor->IsEnabled()) btnExtendMotor->Enable();
			}

			/********************************* OTHER BUTTONS *************************************/

			if (!btnConfirmRadiation->IsEnabled()) btnConfirmRadiation->Enable();
			if (!btnZero->IsEnabled()) btnZero->Enable();

			if (!txtCtrlRadiation->IsEnabled())
			{
				txtCtrlRadiation->Enable();
				txtCtrlRadiation->SetValue(wxString::Format("%G", A_target));
			}

			/********************************* START BUTTON **************************************/

			if ((mbStatusMotorScrewed && mbStatusMotorUnscrewed != mbStatusMotorScrewed) || mbStatusWater) //no errors/good conditions OR water is flowing
			{
				if (!btnStart->IsEnabled()) btnStart->Enable();
			}
			else if ((mbStatusMotorUnscrewed || (mbStatusMotorUnscrewed == mbStatusMotorScrewed)) && !mbStatusWater) //errors/bad conditions AND water is not flowing
			{
				if (btnStart->IsEnabled()) btnStart->Disable();
			}

			/********************************* STOP BUTTON **************************************/

			if (mbStatusWater && mbStatusHeadExtended && mbStatusHeadExtended != mbStatusHeadRetracted && !timerStop->IsRunning()) //EXPERIMENT IS READY
			{
				if (!btnStop->IsEnabled()) btnStop->Enable(); //can be STOPPED
			}
			else if (btnStop->IsEnabled()) btnStop->Disable();

			/********************************* EXPERIMENT BUTTON **************************************/

			if (mbStatusWater && mbStatusHeadExtended && mbStatusHeadExtended != mbStatusHeadRetracted)
			{
				if (!btnExperiment->IsEnabled()) btnExperiment->Enable(); //always available during normal operation
			}
			else if (btnExperiment->IsEnabled()) btnExperiment->Disable();

			if (btnExperiment->IsEnabled())
			{
				if (bExperimentIsRunning) btnExperiment->SetBackgroundColour(wxColour(255, 127, 0)); //orange
				else btnExperiment->SetBackgroundColour(wxColour(0, 255, 0)); //green
			}
			else btnExperiment->SetBackgroundColour(wxColour(220, 220, 220));
		}
	}
	else //LOCAL MODE
	{
		//Disable all user buttons
		if (btnOpenZ1->IsEnabled()) btnOpenZ1->Disable();
		if (btnCloseZ1->IsEnabled()) btnCloseZ1->Disable();
		if (btnOpenZ2->IsEnabled()) btnOpenZ2->Disable();
		if (btnCloseZ2->IsEnabled()) btnCloseZ2->Disable();
		if (btnOpenZ3->IsEnabled()) btnOpenZ3->Disable();
		if (btnCloseZ3->IsEnabled()) btnCloseZ3->Disable();
		if (btnExtendMotor->IsEnabled()) btnExtendMotor->Disable();
		if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
		if (btnStart->IsEnabled()) btnStart->Disable();
		if (btnStop->IsEnabled()) btnStop->Disable();
		if (btnRetractMotor->IsEnabled()) btnRetractMotor->Disable();
		if (btnConfirmRadiation->IsEnabled()) btnConfirmRadiation->Disable();
		if (btnZero->IsEnabled()) btnZero->Disable();
		if (btnExperiment->IsEnabled()) btnExperiment->Disable();
		btnExperiment->SetBackgroundColour(wxColour(220, 220, 220));

		//Disable text input
		txtCtrlRadiation->SetValue(_T("-"));
		if (txtCtrlRadiation->IsEnabled()) txtCtrlRadiation->Disable();
	}

	/********************************* VALVE/MOTOR SAFETY ********************************/

	//Re-enable button functionality after all commands were sent and a reply received
	if (bAwaitingReply && mbMaster->bReplyReceived && bWriteOperationComplete)
	{
		//Clear flags
		mbMaster->bReplyReceived = false;
		bAwaitingReply = false;
	}

	/********************************* VALVE STATUSES *************************************/

	//Valve Z1
	if (mbStatusValves[0]) //OPEN
	{
		txtZ1Status->SetForegroundColour(wxColour(0, 200, 0));
		txtZ1Status->SetLabel(_T("OTWARTY"));
	}
	else //CLOSED
	{
		txtZ1Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ1Status->SetLabel(_T("ZAMKNIĘTY"));
	}

	//Valve Z2
	if (mbStatusValves[1]) //OPEN
	{
		txtZ2Status->SetForegroundColour(wxColour(0, 200, 0));
		txtZ2Status->SetLabel(_T("OTWARTY"));
	}
	else //CLOSED
	{
		txtZ2Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ2Status->SetLabel(_T("ZAMKNIĘTY"));
	}

	//Valve Z3
	if (mbStatusValves[2]) //OPEN
	{
		txtZ3Status->SetForegroundColour(wxColour(0, 200, 0));
		txtZ3Status->SetLabel(_T("OTWARTY"));
	}
	else //CLOSED
	{
		txtZ3Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ3Status->SetLabel(_T("ZAMKNIĘTY"));
	}

	//Valve Z4
	if (mbStatusValves[3]) //OPEN
	{
		txtZ4Status->SetForegroundColour(wxColour(0, 200, 0));
		txtZ4Status->SetLabel(_T("OTWARTY"));
	}
	else //CLOSED
	{
		txtZ4Status->SetForegroundColour(wxColour(220, 0, 0));
		txtZ4Status->SetLabel(_T("ZAMKNIĘTY"));
	}

	/********************************** MOTOR STATUS **************************************/

	if (mbStatusMotorScrewed == mbStatusMotorUnscrewed) //ERROR
	{
		txtMotorScrewedStatus->SetForegroundColour(wxColour(220, 0, 0));
		txtMotorScrewedStatus->SetLabel(_T("BŁĄD"));
	}
	else if (mbStatusMotorScrewed) //SCREWED
	{
		txtMotorScrewedStatus->SetForegroundColour(wxColour(255, 127, 0)); //orange
		txtMotorScrewedStatus->SetLabel(_T("ZAMKNIĘTA"));
	}
	else if (mbStatusMotorUnscrewed) //UNSCREWED
	{
		txtMotorScrewedStatus->SetForegroundColour(wxColour(0, 200, 0));
		txtMotorScrewedStatus->SetLabel(_T("OTWARTA"));
	}

	/********************************** HEAD STATUS ***************************************/

	if (mbStatusHeadExtended == mbStatusHeadRetracted)
	{
		if (imgHeadRetracted->IsShown()) imgHeadRetracted->Hide();
		if (imgHeadExtended->IsShown()) imgHeadExtended->Hide();
	}
	else if (mbStatusHeadExtended) //EXTENDED
	{
		if (imgHeadRetracted->IsShown()) imgHeadRetracted->Hide();
		if (!imgHeadExtended->IsShown()) imgHeadExtended->Show();
	}
	else if (mbStatusHeadRetracted)  //RETRACTED
	{
		if (imgHeadExtended->IsShown()) imgHeadExtended->Hide();
		if (!imgHeadRetracted->IsShown()) imgHeadRetracted->Show();
	}

	/********************************** WATER STATUS ***************************************/

	if (mbStatusWater)
	{
		if (!imgWater->IsShown()) imgWater->Show(); //OK
	}
	else if (imgWater->IsShown()) imgWater->Hide(); //NOK

	/********************************** MEASUREMENTS ***************************************/

	if (!bRefreshDisplay) bRefreshDisplay = true;
	I = (((float) mbReadingCurrent * ADC_FACTOR) / ADC_SCALE_CURRENT) / 1E9;
	T = ((double) mbReadingTemperature * ADC_FACTOR) / ADC_SCALE_TEMP;
}

void cMain::RefreshDisplay(void)
{
	if (bRefreshDisplay)
	{
		if (bExperimentIsRunning)
		{
			//Calculate target sample
			Np_target = A_target/lambda;

			//Calculate sample production
			Np += Kp*I*dt - lambda*Np_tminus*dt;
			if (Np < 0) Np = 0;

			//Calculate activity
			A = lambda*Np;

			//Calculate time to completion
			delta_Np = (Kp*I - lambda*Np_tminus)*dt;

			if (delta_Np > 0 && Np < Np_target)
			{
				timeToCompletionSec = ((Np_target - Np) / delta_Np) * dt;
				txtBoxCurrentRadiation->SetForegroundColour(wxColour(0, 0, 220)); //navy blue
			}
			else if (Np >= Np_target)
			{
				timeToCompletionSec = 0;
				txtBoxCurrentRadiation->SetForegroundColour(wxColour(220, 60, 0)); // orange
			}
			else if (delta_Np <= 0)
			{
				timeToCompletionSec = 0;
				txtBoxCurrentRadiation->SetForegroundColour(wxColour(0, 0, 220)); //navy blue
			}

			//Update amount of sample already produced
			Np_tminus = Np;

			//Get time elapsed since start of experiment
			timeElapsedMs = stopwatch.Time();

			//Update elapsed time display
			timeElapsedHour = (timeElapsedMs/1000)/3600; //round down (truncate)
			timeElapsedMin = (timeElapsedMs/1000)/60 - timeElapsedHour*60; //round down (truncate)
			timeElapsedSec = timeElapsedMs/1000 - timeElapsedMin*60 - timeElapsedHour*3600;

			if (timeElapsedHour < 10) strTimeElapsedHour = wxString::Format("0%d:", timeElapsedHour);
			else strTimeElapsedHour = wxString::Format("%d:", timeElapsedHour);
			if (timeElapsedMin < 10) strTimeElapsedMin = wxString::Format("0%d:", timeElapsedMin);
			else strTimeElapsedMin = wxString::Format("%d:", timeElapsedMin);
			if (timeElapsedSec < 10) strTimeElapsedSec = wxString::Format("0%d", timeElapsedSec);
			else strTimeElapsedSec = wxString::Format("%d", timeElapsedSec);
			strTimeElapsed = wxString(strTimeElapsedHour + strTimeElapsedMin + strTimeElapsedSec);

			timeToCompletionRefresh += dt;

			//Update time to completion display every TIME_TO_COMPLETION_REFRESH seconds
			if (timeToCompletionRefresh >= TIME_TO_COMPLETION_REFRESH)
			{
				if (delta_Np > 0 && Np < Np_target)
				{
					txtBoxTimeToCompletion->SetForegroundColour(wxColour());
				}
				else if (Np >= Np_target)
				{
					txtBoxTimeToCompletion->SetForegroundColour(wxColour());
				}
				else if (delta_Np <= 0)
				{
					txtBoxTimeToCompletion->SetForegroundColour(wxColour(220, 0, 0));
				}

				timeToCompletionRefresh = 0;
				timeToCompletionHour = (timeToCompletionSec)/3600; //round down (truncate)
				timeToCompletionMin = (timeToCompletionSec)/60 - timeToCompletionHour*60; //round down (truncate)
				timeToCompletionSec = timeToCompletionSec - timeToCompletionMin*60 - timeToCompletionHour*3600; //round down (truncate)
				if (timeToCompletionHour < 10) strTimeToCompletionHour = wxString::Format("0%d:", timeToCompletionHour);
				else strTimeToCompletionHour = wxString::Format("%d:", timeToCompletionHour);
				if (timeToCompletionMin < 10) strTimeToCompletionMin = wxString::Format("0%d:", timeToCompletionMin);
				else strTimeToCompletionMin = wxString::Format("%d:", timeToCompletionMin);
				if (timeToCompletionSec < 10) strTimeToCompletionSec = wxString::Format("0%d", (uint32_t) timeToCompletionSec);
				else strTimeToCompletionSec = wxString::Format("%d", (uint32_t) timeToCompletionSec);

				if (timeToCompletionHour > 10) strTimeToCompletion = wxString(_T(">10:00:00"));
				else strTimeToCompletion = wxString(strTimeToCompletionHour + strTimeToCompletionMin + strTimeToCompletionSec);
			}
		}

		//Update experiment result
		if (A >= 1E9) txtBoxCurrentRadiation->SetLabel(wxString::Format("%.2f GBq", A/1E9));
		else if (A >= 1E6) txtBoxCurrentRadiation->SetLabel(wxString::Format("%.2f MBq", A/1E6));
		else if (A >= 1E3) txtBoxCurrentRadiation->SetLabel(wxString::Format("%.2f KBq", A/1E3));
		else txtBoxCurrentRadiation->SetLabel(wxString::Format("%.2f Bq", A));

		//Update time elapsed and time to completion
		txtBoxTimeElapsed->SetLabel(strTimeElapsed);
		txtBoxTimeToCompletion->SetLabel(strTimeToCompletion);

		//Update temperature reading
		if ((T * ADC_SCALE_TEMP) < TEMPERATURE_MIN)
		{
			strTemperature = wxString(_T("Błąd"));
			txtBoxTemperature->SetLabel(strTemperature);
		}
		else
		{
			strTemperature = wxString::Format("%d ", (uint16_t) round(T));
			txtBoxTemperature->SetLabel(strTemperature + _T("\x2103"));
		}

		//Update current reading
		txtBoxCurrent->SetLabel(wxString::Format("%.1f ", I*1E9) + _T("nA"));

		//Write current reading to log file
		if (threadSystemClock && bExperimentIsRunning)
		{
			wxCriticalSectionLocker enter(threadSystemClock->clock_guard);
			pEntry->tLogFile.AddLine(wxString::Format("%s  %.1f ", threadSystemClock->time.c_str(), I*1E9));
		}

		//Update pressure measurements
		if (bStatusTPG362)
		{
			wxCriticalSectionLocker enter(tpg362_guard);

			if (P2_status == '3' || P2_status == '5' || P2_status == '6')
			{
				txtBoxPressure->SetForegroundColour(wxColour(220, 0, 0));
				txtBoxPressure->SetPosition(wxPoint(100, 19));
				txtBoxPressure->SetLabel(wxString(_T("Błąd")));
			}
			else if (P2_status == '4')
			{
				txtBoxPressure->SetForegroundColour(wxColour());
				txtBoxPressure->SetPosition(wxPoint(100, 19));
				txtBoxPressure->SetLabel(wxString(_T("Wył.")));
			}
			else
			{
				txtBoxPressure->SetForegroundColour(wxColour());
				txtBoxPressure->SetPosition(wxPoint(100, 19));
				txtBoxPressure->SetLabel(wxString::Format("%.2E\nTorr", P2));
			}

			if (P1_status == '3' || P1_status == '5' || P1_status == '6')
			{
				txtBoxPressureZ1->SetForegroundColour(wxColour(220, 0, 0));
				txtBoxPressureZ1->SetPosition(wxPoint(8, 180));
				txtBoxPressureZ1->SetLabel(wxString(_T("Błąd")));
			}
			else if (P1_status == '4')
			{
				txtBoxPressureZ1->SetForegroundColour(wxColour());
				txtBoxPressureZ1->SetPosition(wxPoint(8, 180));
				txtBoxPressureZ1->SetLabel(wxString(_T("Wył.")));
			}
			else
			{
				txtBoxPressureZ1->SetForegroundColour(wxColour());
				txtBoxPressureZ1->SetPosition(wxPoint(8, 175));
				txtBoxPressureZ1->SetLabel(wxString::Format("%.2E\nTorr", P1));
			}
		}
		else
		{
			txtBoxPressure->SetForegroundColour(wxColour());
			txtBoxPressureZ1->SetForegroundColour(wxColour());
			txtBoxPressure->SetLabel(wxString(_T("---")));
			txtBoxPressureZ1->SetPosition(wxPoint(8, 180));
			txtBoxPressureZ1->SetLabel(wxString(_T("---")));
		}
	}
}

void cMain::OnRefreshDisplay(wxCommandEvent& evt)
{
	RefreshDisplay();
}

/* System clock event handler */
void cMain::OnClockUpdate(wxCommandEvent& evt)
{
	if (threadSystemClock) mMenuBar->SetMenuLabel(0, threadSystemClock->time);
}

/* Menu clicked event */
void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
    if (informationWindowIsOpen) //window already open
    {
        pInformation->Raise(); //show window
    }
    else
    {
        informationWindowIsOpen = true;
        pInformation = new cInformation(this);
        pInformation->SetPosition(this->GetPosition());
        pInformation->SetIcon(this->GetIcon());
        pInformation->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedConstants(wxCommandEvent& evt)
{
    if (constantsWindowIsOpen) //window already open
    {
        pConstants->Raise(); //show window
    }
    else
    {
    	constantsWindowIsOpen = true;
    	pConstants = new cConstants(this);
    	pConstants->SetPosition(this->GetPosition());
    	pConstants->SetIcon(this->GetIcon());
    	pConstants->Show();
    }

    evt.Skip();
}


/* Frame closed - Modbus thread destruction */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp();
}

/* Frame closed from menu - Modbus thread destruction */
void cMain::OnMenuClickedExit(wxCommandEvent& evt)
{
	TerminateApp();
}

void cMain::ClearTimeoutError()
{
	if (bTimeoutError) bTimeoutError = false; //clear timeout error flag
}

void cMain::TerminateApp()
{
	//Stop timers
	if (timerStart->IsRunning()) timerStart->Stop();
	if (timerStop->IsRunning()) timerStop->Stop();
	if (timerMotor->IsRunning()) timerMotor->Stop();
	if (timerZ1OpenToggle->IsRunning()) timerZ1OpenToggle->Stop();
	if (timerZ1CloseToggle->IsRunning()) timerZ1CloseToggle->Stop();
	if (timerZ2Toggle->IsRunning()) timerZ2Toggle->Stop();
	if (timerZ3Toggle->IsRunning()) timerZ3Toggle->Stop();
	if (timerModbusWriteError->IsRunning()) timerModbusWriteError->Stop();

	//Delete TPG362 thread
	if (threadTPG362)
	{
		if (threadTPG362->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete TPG362 thread!");
		wxThread::This()->Sleep(TCP_REFRESH_TIME);
	}

	//Delete display refresh thread
	if (threadRefreshDisplay)
	{
		if (threadRefreshDisplay->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete display refresh thread!");
	}

	while (1)
	{
		if (!threadRefreshDisplay) break;
		wxThread::This()->Sleep(1);
	}

	//Delete system clock thread
	if (threadSystemClock)
	{
		if (threadSystemClock->Delete() != wxTHREAD_NO_ERROR) wxLogError("Can't delete system clock thread!");
	}

	while (1)
	{
		if (!threadSystemClock) break;
		wxThread::This()->Sleep(1);
	}

	//Close information window
	if (informationWindowIsOpen) pInformation->Close();
	if (constantsWindowIsOpen) pConstants->Close();

	//Close and copy log file
	pEntry->tLogFile.Write();
	pEntry->tLogFile.Close();
	system(wxString((wxString("cp \'/dev/shm/") + pEntry->tLogFileName + wxString("\' ") + wxString::FromUTF8(getenv("HOME")) +
			wxString("\'/eclipse-workspace/NaswietlanieWiazka/Log/") + pEntry->tLogFileName + wxString("\'"))).c_str());

	Destroy();
}
