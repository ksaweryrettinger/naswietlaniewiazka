#include "cThreadSystemClock.h"

DEFINE_EVENT_TYPE(wxEVT_CLOCK_UPDATE)

cThreadSystemClock::cThreadSystemClock(cMain* pMain) : wxThread(wxTHREAD_DETACHED)
{
	this->pMain = pMain;
}

cThreadSystemClock::~cThreadSystemClock()
{
	pMain->threadSystemClock = NULL;
}

wxThread::ExitCode cThreadSystemClock::Entry()
{
    while (!TestDestroy())
    {
    	now = wxDateTime::Now();

    	{
			wxCriticalSectionLocker enter(clock_guard);

			if (now.GetHour() < 10) time = wxString::Format("0%d:", now.GetHour());
			else time = wxString::Format("%d:", now.GetHour());

			if (now.GetMinute() < 10) time += wxString::Format("0%d:", now.GetMinute());
			else time += wxString::Format("%d:", now.GetMinute());

			if (now.GetSecond() < 10) time += wxString::Format("0%d", now.GetSecond());
			else time += wxString::Format("%d", now.GetSecond());
    	}

		if (pMain != nullptr)
		{
			//Send event to main window
			wxCommandEvent* evt = new wxCommandEvent(wxEVT_CLOCK_UPDATE, wxID_ANY);
			((wxEvtHandler*) pMain)->QueueEvent(evt);
		}

    	wxThread::Sleep(CLOCK_REFRESH_TIME);
    }

	return (wxThread::ExitCode) 0;
}
