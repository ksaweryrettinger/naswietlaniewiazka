#include "cInformation.h"

wxBEGIN_EVENT_TABLE(cInformation, wxFrame)
	EVT_CLOSE(cInformation::OnClose)
wxEND_EVENT_TABLE()

cInformation::cInformation(cMain* pMain) : wxFrame(nullptr, wxID_ANY, _T("Informacje"), wxPoint(390, 300), wxSize(360, 250), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    this->pMain = pMain;

	//Set window background colour
	this->SetBackgroundColour(wxColour(235, 235, 235));

	//Initialize static text
	txtInformation = new wxStaticText(this, wxID_ANY,
		_T("\n\nNaświetlanie Wiązką Wewnętrzną\n\nKsawery Wieczorkowski-Rettinger\n\nPracownia Elektroniczna (Pokój 102)\n\nŚrodowiskowe Laboratorium Ciężkich Jonów\n\nkwrettinger@gmail.com\n\n+48 664 998 588"),
		wxPoint(40, 0), wxSize(110, 30), wxALIGN_CENTRE_HORIZONTAL);

	myFont = wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	if (txtInformation != nullptr) txtInformation->SetFont(myFont);
}

/* Diagnostics frame closed */
void cInformation::OnClose(wxCloseEvent& evt)
{
	pMain->informationWindowIsOpen = false;
	Destroy();
}
