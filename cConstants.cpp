#include "cConstants.h"

wxBEGIN_EVENT_TABLE(cConstants, wxFrame)
	EVT_CLOSE(cConstants::OnClose)
	EVT_BUTTON(10022, cConstants::OnConstantsChanged)
wxEND_EVENT_TABLE()

cConstants::cConstants(cMain* pMain) : wxFrame(nullptr, wxID_ANY, _T("Parametry Produkcji"), wxPoint(390, 300), wxSize(480, 177), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    this->pMain = pMain;

	//Set window background colour
	this->SetBackgroundColour(wxColour(235, 235, 235));

	//Initialize static text
    txtCtrlProductionConstant = new wxTextCtrl(this, wxID_ANY, "", wxPoint(165, 34), wxSize(120, 31), wxALIGN_CENTRE_HORIZONTAL);
    txtTitleProductionConstant = new wxStaticText(this, wxID_ANY, _T("Stała Produkcji:"), wxPoint(15, 40), wxSize(150, 30), wxALIGN_CENTRE_HORIZONTAL);
    txtUnitsProductionConstant = new wxStaticText(this, wxID_ANY, _T("A\x207b\x00b9 s\x207b\x00b9"), wxPoint(295, 41), wxSize(55, 30), wxALIGN_LEFT);

    txtCtrlDecayConstant = new wxTextCtrl(this, wxID_ANY, "", wxPoint(165, 84), wxSize(120, 31), wxALIGN_CENTRE_HORIZONTAL);
    txtTitleDecayConstant = new wxStaticText(this, wxID_ANY, _T("Stała Rozpadu (λ):"), wxPoint(15, 90), wxSize(150, 40), wxALIGN_CENTRE_HORIZONTAL);
    txtUnitsDecayConstant = new wxStaticText(this, wxID_ANY, _T("s\x207b\x00b9"), wxPoint(295, 91), wxSize(20, 30), wxALIGN_LEFT);

    //Initialize button
    btnConfirmConstants = new wxButton(this, 10022, _T("Potwierdź"), wxPoint(365, 60), wxSize(85, 30));
    btnConfirmConstants->SetBackgroundColour(wxColour(220, 220, 220));

    //Constants already set - initialize values in window
    if (pMain->Kp >= 0) txtCtrlProductionConstant->SetValue(wxString::Format(wxT("%G"), pMain->Kp));
    if (pMain->lambda >= 0) txtCtrlDecayConstant->SetValue(wxString::Format(wxT("%G"), pMain->lambda));
}

void cConstants::OnConstantsChanged(wxCommandEvent& evt)
{
	/********************************* Production Constant ******************************/

	//Get new value
	wxString strNewConstant = txtCtrlProductionConstant->GetValue();
	float newConstant = wxAtof(strNewConstant);

	//Update display
	if (newConstant < PRODUCTION_CONST_LIMIT_LOWER) newConstant = PRODUCTION_CONST_LIMIT_LOWER;
	else if (newConstant > PRODUCTION_CONST_LIMIT_UPPER) newConstant = PRODUCTION_CONST_LIMIT_UPPER;
	txtCtrlProductionConstant->SetValue(wxString::Format(wxT("%G"), newConstant));
	pMain->Kp = newConstant;

	/********************************* Decay Constant ***********************************/

	//Get new value
	strNewConstant = txtCtrlDecayConstant->GetValue();
	newConstant = wxAtof(strNewConstant);

	//Update display
	if (newConstant < DECAY_CONST_LIMIT_LOWER) newConstant = DECAY_CONST_LIMIT_LOWER;
	else if (newConstant > DECAY_CONST_LIMIT_UPPER) newConstant = DECAY_CONST_LIMIT_UPPER;
	txtCtrlDecayConstant->SetValue(wxString::Format(wxT("%G"), newConstant));
	pMain->lambda = newConstant;


	wxString constants = _T("Stała Produkcji = ") + wxString::Format("%.2E, ", pMain->Kp) +
			_T("Stała Rozpadu = ") + wxString::Format("%.2E", pMain->lambda);

	pMain->pEntry->tLogFile.RemoveLine(2);
	pMain->pEntry->tLogFile.InsertLine(constants, 2);
	pMain->pEntry->tLogFile.Write();
}

/* Constants frame closed */
void cConstants::OnClose(wxCloseEvent& evt)
{
	pMain->constantsWindowIsOpen = false;
	Destroy();
}
