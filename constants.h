#pragma once

#include "wx/wx.h"
#include <limits.h>

//ADC SCALING FACTORS
const float ADC_FACTOR = 3298.0f / 4096.0f; //3298 mV/MAX
const float ADC_SCALE_CURRENT = 30.0f / 4.0f; //3V/400nA -> 3000mV/400nA -> 30mV/4nA
const float ADC_SCALE_TEMP = 40.0f; // 40mV / 1C, 100mV min
const uint8_t TEMPERATURE_MIN = 100;

//MODBUS
const int32_t MB_BAUD = 19200;
const int32_t MB_RESPONSE_TIMEOUT_SEC = 0;
const int32_t MB_RESPONSE_TIMEOUT_US = 75000;
const uint32_t MB_SCANRATE_MS = 250;
const int32_t MB_NUM_COILS = 8;
const int32_t MB_NUM_INPUT_BITS = 12;
const int32_t MB_NUM_INPUT_REGISTERS = 30;
const int32_t MB_NUM_REGISTERS = 0;

//OTHER TIMEOUTS (ms)
const int32_t Z1_OPEN_TOGGLE_TIMEOUT = 500;
const int32_t Z1_CLOSE_TOGGLE_TIMEOUT = 500;
const int32_t Z2_TOGGLE_TIMEOUT = 500;
const int32_t Z3_TOGGLE_TIMEOUT = 500;
const int32_t MOTOR_TIMEOUT = 60000;
const int32_t START_TIMEOUT = 15000;
const int32_t STOP_TIMEOUT = 30000;
const int32_t MODBUS_WRITE_ERROR_TIMEOUT = 2500;
const int32_t EXPERIMENT_REFRESH_TIMEOUT = 1000;

//THREAD REFRESH TIMES
const int32_t TCP_REFRESH_TIME = 100;
const int32_t CLOCK_REFRESH_TIME = 1000;
const int32_t DISPLAY_REFRESH_TIME = 1000;

//COIL ADDRESING
const int32_t COIL_OPEN_Z1 = 1;
const int32_t COIL_CLOSE_Z1 = 2;
const int32_t COIL_TOGGLE_Z2 = 3;
const int32_t COIL_TOGGLE_Z3 = 4;
const int32_t COIL_EXTEND_HEAD = 5;
const int32_t COIL_RETRACT_HEAD = 6;
const int32_t COIL_START = 7;
const int32_t COIL_STOP = 8;

//TEXT INPUT SAFETY
const float RADIATION_LIMIT_UPPER = std::numeric_limits<float>::max();
const float RADIATION_LIMIT_LOWER = 0;
const float PRODUCTION_CONST_LIMIT_UPPER = std::numeric_limits<float>::max();;
const float PRODUCTION_CONST_LIMIT_LOWER = 0;
const float DECAY_CONST_LIMIT_UPPER = std::numeric_limits<float>::max();;
const float DECAY_CONST_LIMIT_LOWER = 0;

//OTHER
const uint32_t AVERAGE_CURRENT_READINGS = 5;
const uint32_t TIME_TO_COMPLETION_REFRESH = 5;
