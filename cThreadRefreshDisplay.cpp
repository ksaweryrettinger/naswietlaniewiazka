#include "cThreadRefreshDisplay.h"

DEFINE_EVENT_TYPE(wxEVT_DISPLAY_UPDATE)

cThreadRefreshDisplay::cThreadRefreshDisplay(cMain* pMain) : wxThread(wxTHREAD_DETACHED)
{
	this->pMain = pMain;
}

cThreadRefreshDisplay::~cThreadRefreshDisplay()
{
	pMain->threadRefreshDisplay = NULL;
}

wxThread::ExitCode cThreadRefreshDisplay::Entry()
{
    while (!TestDestroy())
    {
		if (pMain != nullptr)
		{
			//Send event to main window
			wxCommandEvent* evt = new wxCommandEvent(wxEVT_DISPLAY_UPDATE, wxID_ANY);
			((wxEvtHandler*) pMain)->QueueEvent(evt);
		}

    	wxThread::Sleep(DISPLAY_REFRESH_TIME);
    }

	return (wxThread::ExitCode) 0;
}
