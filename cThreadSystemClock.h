#ifndef CTHREADSYSTEMCLOCK_H_
#define CTHREADSYSTEMCLOCK_H_

#include "cMain.h"

DECLARE_EVENT_TYPE(wxEVT_CLOCK_UPDATE, -1)

/* System clock thread */
class cThreadSystemClock : public wxThread
{
public:
	cThreadSystemClock(cMain*);
	~cThreadSystemClock();

public:
	wxCriticalSection clock_guard;

protected:
    virtual ExitCode Entry();

private:
    cMain* pMain;
	wxString time;
	wxDateTime now;

	friend class cMain;
};

#endif
