#include "cResetCoils.h"

wxBEGIN_EVENT_TABLE(cResetCoils, wxFrame)
	EVT_BUTTON(10030, cResetCoils::OnButtonResetCoils)
	EVT_BUTTON(10031, cResetCoils::OnButtonOK)
	EVT_CLOSE(cResetCoils::OnClose)
wxEND_EVENT_TABLE()

cResetCoils::cResetCoils(cMain* pMain, cModbus* pModbus) : wxFrame(nullptr, wxID_ANY, _T("Stan Pinów Wyjściowych"), wxPoint(390, 300), wxSize(445, 240), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
	this->pMain = pMain;
    this->mbMaster = pModbus;
    sumCoils = 0;
    rc = -1;

	//Set window background colour
	this->SetBackgroundColour(wxColour(235, 235, 235));

	//Information text
	txtInformation = new wxStaticText(this, wxID_ANY,
			_T("Uwaga: niektóre wyjścia z komputera do PLC są w stanie\n"
			   "wysokim - stanowisko może być w trakcie wykonywania\n"
			   "polecenia (np. zmiana pozycji głowicy do naświetleń).\n\n"
			   "Aby rozpocząc pracę od początku, należy zresetować \n"
			   "wszystkie wyjścia do PLC. Przed resetem, upewnij\n"
			   "się, że ostatnie polecenie zostało wykonane\n"
			   "(niektóre polecenia mogą trwać 60s)."),
		wxPoint(0, 20), wxSize(450, 60), wxALIGN_CENTRE_HORIZONTAL);
	myFont = wxFont(9, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
	txtInformation->SetFont(myFont);

	//Modbus error text
    txtUserMessage = new wxStaticText(this, wxID_ANY, _T("Błąd."), wxPoint(32, 167), wxSize(400, 20), wxALIGN_LEFT);
    myFont = wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    txtUserMessage->SetFont(myFont);
    txtUserMessage->SetForegroundColour(wxColour(220, 0, 0));
    txtUserMessage->Hide();

	//Create buttons
    btnResetCoils = new wxButton(this, 10030, _T("Resetuj Wyjścia"), wxPoint(78, 160), wxSize(120, 30));
    btnOK = new wxButton(this, 10031, _T("Kontynuuj Bez Resetu"),  wxPoint(218, 160), wxSize(154, 30));
    btnResetCoils->SetBackgroundColour(wxColour(220, 220, 220));
    btnOK->SetBackgroundColour(wxColour(220, 220, 220));
}

/* Diagnostics frame closed */
void cResetCoils::OnButtonResetCoils(wxCommandEvent& evt)
{
	//Enter critical section
	wxCriticalSectionLocker enter(mbMaster->mb_guard);
	rc = modbus_write_bits(mbMaster->ctx, 1, MB_NUM_COILS, zero);

	if (rc != -1)
	{
		rc = modbus_read_bits(mbMaster->ctx, 1, MB_NUM_COILS, mbMaster->mb_mapping->tab_bits);

		if (rc != -1)
		{
			for (int i = 0; i < MB_NUM_COILS; i++) sumCoils += mbMaster->mb_mapping->tab_bits[i];

			if (sumCoils == 0)
			{
				btnResetCoils->Disable();
				pMain->Show();
				Destroy();
			}
			else txtUserMessage->Show();
		}
		else txtUserMessage->Show();
	}
	else txtUserMessage->Show();
}

/* Diagnostics frame closed */
void cResetCoils::OnButtonOK(wxCommandEvent& evt)
{
	pMain->Show();
	Destroy();
}

/* Diagnostics frame closed */
void cResetCoils::OnClose(wxCloseEvent& evt)
{
	pMain->Show();
	Destroy();
}
